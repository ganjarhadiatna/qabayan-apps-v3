import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
// import 'package:qabayan_ui/posts/index.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/popupAlert.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
import 'package:qabayan_ui/assets/popupQuestion.dart';

class CardBergainSalesWidget extends StatelessWidget {

  final int val;

  CardBergainSalesWidget({Key key, @required this.val}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        new SizedBox(height: 15),
        _onCardLeft(context, "Rp 15.000", false),
        _onCardLeft(context, "Rp 10.000", true),
        _onCardLeft(context, "Rp 13.000", true),
        _onCardLeft(context, "Rp 14.000", true),
      ],
    );
  }


  _onCardLeft (BuildContext context, var price, var val) {

    return new InkWell(
      onLongPress: () {
        _onMore(context);
      },
      child: new Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            new Container(
              margin: EdgeInsets.only(right: 10),
              width: 40,
              height: 40,
              color: Colors.white,
              child: ClipRRect(
                borderRadius: new BorderRadius.circular(5),
                child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
              ),
            ),
            
            new Expanded(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                    margin: EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      )
                    ),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text('Green Tea', style: TextStyle(
                          color: OnColors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold
                        )),
                        new SizedBox(height: 5),
                        _onInfo('Negosiasi', price, OnColors.tertiary),
                        new SizedBox(height: 2.5),
                        _onInfo('Status', val ? 'Menunggu konfirmasi' : 'Disetujui', OnColors.black),
                        val ? (
                          _onFooter(context)
                        ) : new Container()

                      ],
                    ),
                  ),

                  new Container(
                    child: new Text(
                      '5 menit yang lalu',
                      style: TextStyle(color: OnColors.lightGrey, fontSize: 14),
                    ),
                  )
                ],
              ),
            )

          ],
        ),
      )
    );
  }

  _onFooter (BuildContext context) {
    return new Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new SizedBox(height: 15),
            new Text('Setuju pada penawaran yang diberikan?', style: TextStyle(
              color: OnColors.black,
              fontSize: 16,
              fontWeight: FontWeight.normal
            )),
            new SizedBox(height: 5),
            new Container(
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Expanded(
                    child: ButtonText(
                      title: 'Tidak',
                      action: () {
                        _onDecline(context);
                      },
                      type: 'grey',
                    ),
                  ),
                  new SizedBox(width: 10),
                  new Expanded(
                    child: ButtonText(
                      title: 'Ya, setuju',
                      action: () {
                        _onApproval(context);
                      },
                      type: 'grey',
                    ),
                  )
                ],
              ),
          )
        ],
      ),
    );
  }

  _onInfo (var title, var value, var valColor) {
    return new Container(
      width: double.infinity,
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[

          new Text(
            title,
            style: TextStyle(
              color: Colors.grey.shade800,
              fontWeight: FontWeight.normal,
              fontSize: 16
            ),
          ),

          new Text(
            value,
            style: TextStyle(
              color: valColor,
              fontWeight: FontWeight.bold,
              fontSize: 16
            ),
          ),

        ],
      ),
    );
  }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Pengaturan',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              action: () {
                Navigator.of(context).pushNamed('user-profile');
              },
              icon: LineIcons.user,
              type: 'white',
              title: 'Lihat pengguna',
            ),
            new SizedBox(height: 15),
            OnLine(),
            new SizedBox(height: 15),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

  _onDecline (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupQuestion(
        title: 'Konfirmasi',
        subtitle: 'Tolak negosiasi yang diberikan pengguna ini?',
        action: () {
          Navigator.pop(context);
          showDialog(
            context: context,
            builder: (BuildContext context) => PopupAlert(
              title: 'Pemberitahuan',
              subtitle: 'Negosiasi ini sudah ditolak.',
              action: () {
                Navigator.pop(context);
              },
            )
          );
        }
      )
    );
  }

  _onApproval (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupQuestion(
        title: 'Konfirmasi',
        subtitle: 'Setujui negosiasi yang diberikan pengguna ini?',
        action: () {
          Navigator.pop(context);
          showDialog(
            context: context,
            builder: (BuildContext context) => PopupAlert(
              title: 'Pemberitahuan',
              subtitle: 'Negosiasi berhasil disetujui.',
              action: () {
                Navigator.pop(context);
              },
            )
          );
        }
      )
    );
  }

}