import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/onLine.dart';
// import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/views/explore/index.dart';
// import 'package:qabayan_ui/assets/buttonText.dart';
// import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/views/search/cardSearch.dart';
import 'package:qabayan_ui/views/search/result.dart';
// import 'package:qabayan_ui/assets/popupBottom.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    new SizedBox(width: 5),

                    new Expanded(
                      child: new Container(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        width: double.infinity,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: OnColors.whiteGrey,
                        ),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          // mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              margin: EdgeInsets.only(right: 20),
                              child: new Icon(
                                LineIcons.search,
                                color: OnColors.grey,
                                size: 20
                              ),
                            ),
                            new Expanded(
                              child: new Container(
                                child: new TextFormField(
                                keyboardType: TextInputType.text,
                                autofocus: false,
                                textAlign: TextAlign.left,
                                textInputAction: TextInputAction.search,
                                decoration: InputDecoration(
                                  fillColor: OnColors.whiteGrey,
                                  hintText: 'Perlu sayuran apa hari ini?',
                                  contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  border:  OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(0),
                                    borderSide: BorderSide.none,
                                  ),
                                ),
                              ),
                              )
                            )
                          ]
                        )
                      ),
                    ),
                    
                    new SizedBox(width: 20),

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new Container(
        color: Colors.grey.shade100,
        child: new ListView(
          children: <Widget>[
            _onTitle('Riwayat pencarian'),
            CardSearch(
              title: 'Pencarian', 
              icon: Icons.search,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => SearchResultPage(
                      index: 0,
                      title: 'Riwayat Pencarian'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Pencarian', 
              icon: Icons.search,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => SearchResultPage(
                      index: 0,
                      title: 'Riwayat Pencarian'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Pencarian', 
              icon: Icons.search,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => SearchResultPage(
                      index: 0,
                      title: 'Riwayat Pencarian'
                    )
                  )
                );
              }),

            _onTitle('Kategori populer'),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
          ],
        ),
      ),

    );
  }

  _onTitle (var title) {
    return Container (
      margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
      child: Text(
        title,
        style: TextStyle(
          color: OnColors.black,
          fontSize: 16,
          fontWeight: FontWeight.normal,
        )
      ),
    );
  }

}