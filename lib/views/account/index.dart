import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
import 'package:qabayan_ui/assets/titleContent.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/views/explore/index.dart';

import 'package:qabayan_ui/modules/Session.dart';

class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            new SizedBox(width: 20),

            new Expanded(
              child: new Container (
                padding: EdgeInsets.only(top: 17),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      'Akun',
                      style: new TextStyle(
                        color: OnColors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 28
                      ),
                    )
                  ],
                ),
              ),
            ),

            new Container(
              padding: EdgeInsets.only(top: 12.5),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ButtonCircle(
                    action: () {},
                    icon: LineIcons.search,
                    size: 26.0,
                    type: 'white',
                  ),
                  ButtonCircle(
                    action: () { Navigator.of(context).pushNamed('chats'); },
                    icon: LineIcons.comments_o,
                    size: 26.0,
                    type: 'white',
                  ),
                  ButtonCircle(
                    action: () {
                      Navigator.of(context).pushNamed('cart');
                    },
                    icon: LineIcons.shopping_cart,
                    size: 32.0,
                    type: 'white',
                  )
                ]
              )
            ),

            new SizedBox(width: 10)
          ],
        ),
      ),
      body: new Container(
        color: Colors.grey.shade100,
        child: new ListView(
          children: <Widget>[

            // top
            new Container(
              color: Colors.white,
              padding: EdgeInsets.only(top: 15, bottom: 15, left: 20, right: 20),
              margin: EdgeInsets.only(top: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  Text(
                    'Cecep Mastia Setiawan',
                    style: TextStyle(
                      color: OnColors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                    ),
                  ),

                  new SizedBox(height: 10),

                  Text(
                    '+62890000000',
                    style: TextStyle(
                      color: OnColors.lightGrey,
                      fontSize: 16,
                      fontWeight: FontWeight.normal
                    ),
                  ),

                ],
              ),
            ),

            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(bottom: 15),
              child: new Column(
                children: <Widget>[
                  new OnLine(),
                  _onSetting(
                    LineIcons.user, 
                    OnColors.main, 
                    'Lihat Profil', 
                    true, 
                    () {
                      Navigator.of(context).pushNamed('user-profile');
                    }),
                  // _onSetting(
                  //   LineIcons.list, 
                  //   OnColors.main, 
                  //   'Riwayat transaksi', 
                  //   true, 
                  //   () {}),
                ],
              )
            ),


            // barang
            new Container(
              color: Colors.white,
              padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
              child: new TitleContent(
                title: 'Lapak jualan sayur',
                subtitle: '', 
                more: '', 
                action: () {}
              ),
            ),
            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(bottom: 15),
              child: new Column(
                children: <Widget>[
                  new OnLine(),
                  _onSetting(
                    LineIcons.plus,
                    OnColors.main,
                    'Tambah sayuran',
                    true, 
                    () {
                      Navigator.of(context).pushNamed('compose');
                    }),
                  new OnLine(),
                  _onSettingVal(
                    LineIcons.clock_o,
                    OnColors.main,
                    'Sedang Dijual', 
                    '15',
                    () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ExplorePage(
                            index: 0,
                            title: 'Sedang Dijual'
                          )
                        )
                      );
                    }),
                  new OnLine(),
                  _onSettingVal(
                    LineIcons.arrow_circle_o_up, 
                    OnColors.main, 
                    'Paling Laku', 
                    '15',
                    () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ExplorePage(
                            index: 0,
                            title: 'Paling Laku'
                          )
                        )
                      );
                    }),
                  new OnLine(),
                  _onSettingVal(
                    LineIcons.th_list, 
                    OnColors.main, 
                    'Belum Dipublikasi', 
                    '3',
                    () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ExplorePage(
                            index: 0,
                            title: 'Belum Dipublikasi'
                          )
                        )
                      );
                    }),
                ]
              )
            ),


            // pembelian
            new Container(
              color: Colors.white,
              padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
              child: new TitleContent(
                title: 'Pembelian',
                subtitle: '', 
                more: '', 
                action: () {}
              ),
            ),
            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(bottom: 15),
              child: new Column(
                children: <Widget>[
                  new OnLine(),
                  _onSetting(
                    LineIcons.check_circle, 
                    OnColors.main, 
                    'Sayuran sudah dibeli', 
                    true, 
                    () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ExplorePage(
                            index: 0,
                            title: 'Sudah Dibeli'
                          )
                        )
                      );
                    }),
                  new OnLine(),
                  _onSetting(
                    LineIcons.minus_circle,
                    OnColors.main, 
                    'Sayuran batal dibeli', 
                    true,
                    () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ExplorePage(
                            index: 0,
                            title: 'Batal Dibeli'
                          )
                        )
                      );
                    }),
                ]
              )
            ),


            // sosial
            new Container(
              color: Colors.white,
              padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
              child: new TitleContent(
                title: 'Sosial',
                subtitle: '', 
                more: '', 
                action: () {}
              ),
            ),
            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(bottom: 15),
              child: new Column(
                children: <Widget>[
                  new OnLine(),
                  _onSetting(
                    Icons.compare_arrows, 
                    OnColors.main, 
                    'Negosiasi harga', 
                    true, 
                    () {
                      Navigator.of(context).pushNamed('bergains');
                    }),
                  new OnLine(),
                  _onSetting(
                    LineIcons.comments,
                    OnColors.main, 
                    'Diskusi pelanggan', 
                    true,
                    () {
                      Navigator.of(context).pushNamed('chats');
                    }),
                  // _onSetting(
                  //   LineIcons.user_plus, 
                  //   OnColors.main, 
                  //   'Penawaran Barang', 
                  //   true, 
                  //   () {}),
                ]
              )
            ),


            // lainnya
            new Container(
              color: Colors.white,
              padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
              child: new TitleContent(
                title: 'Lainnya',
                subtitle: '', 
                more: '', 
                action: () {}
              ),
            ),
            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(bottom: 15),
              child: new Column(
                children: <Widget>[
                  new OnLine(),
                  _onSetting(
                    LineIcons.gear, 
                    OnColors.main, 
                    'Pengaturan', 
                    true, 
                    () {
                      Navigator.of(context).pushNamed('user-setting'); 
                    }),
                  new OnLine(),
                  _onSetting(
                    LineIcons.info_circle,
                    OnColors.main, 
                    'Tentang Kami', 
                    false, 
                    () {
                      Navigator.of(context).pushNamed('welcome');
                    }),
                ],
              )
            ),

            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(bottom: 15),
              child: new Column(
                children: <Widget>[
                  _onSetting(
                    LineIcons.power_off, 
                    OnColors.red, 
                    'Logout', 
                    false, 
                    () {
                      _onLogout(context);
                    }),
                ],
              )
            ),



          ],
        )
      )
    );
  }

  _onLogout (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Logout',
        content: new Column(
          children: <Widget>[
            ButtonText(
              action: () {
                Session.removeUserID();
                Session.removeUserToken();
                Session.removeUserUsername();
                Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => false);
              },
              type: 'red',
              title: 'Logout akun?'
            ),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'white',
              title: 'Kembali'
            ),
          ]
        ),
      ),
    );
  }


  _onSettingVal (var icon, var icnColor, var title, var val, var action) {
    return new Column(
      children: <Widget>[
        new MaterialButton(
          padding: EdgeInsets.only(top: 17.5, bottom: 17.5, left: 10, right: 10),
          onPressed: action,
          child: new Row(
            children: <Widget>[
              Container(
                width: 40,
                child: Icon(
                  icon,
                  size: 20,
                  color: icnColor,
                ),
              ),
              Text(
                title,
                style: TextStyle(
                  color: Colors.grey.shade800,
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                ),
              ),
              
              Expanded(
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      val,
                      style: TextStyle(
                        color: OnColors.main,
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Container(
                      width: 10,
                    )
                  ],
                ),
              )

            ],
          ),
        )
      ],
    );
  }

  _onSetting (var icon, var icnColor, var title, bool icnRight, var action) {
    Widget icnIcn;

    if (icnRight) {
      icnIcn = Expanded(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.arrow_forward_ios,
              size: 16,
              color: Colors.grey.shade600,
            ),
            Container(
              width: 10,
            )
          ],
        ),
      ); 
    } else {
      icnIcn = Container();
    }

    return new Column(
      children: <Widget>[
        new MaterialButton(
          padding: EdgeInsets.only(top: 17.5, bottom: 17.5, left: 10, right: 10),
          
          onPressed: action,
          child: new Row(
            children: <Widget>[
              Container(
                width: 40,
                child: Icon(
                  icon,
                  size: 20,
                  color: icnColor,
                ),
              ),
              Text(
                title,
                style: TextStyle(
                  color: Colors.grey.shade800,
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                ),
              ),
              icnIcn
            ],
          ),
        )
      ],
    );
  }
}