import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
// import 'package:qabayan_ui/posts/index.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';

class CardChatsWidget extends StatelessWidget {

  final int val;

  CardChatsWidget({Key key, @required this.val}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(top: 15),
      children: <Widget>[
        _onTitle('Hari ini'),
        _onCard(context, 0),
        _onCard(context, 0),
        _onTitle('Kemarin'),
        _onCard(context, 0),
      ],
    );
  }

  _onTitle (var title) {
    return Container (
      margin: EdgeInsets.fromLTRB(20, 0, 20, 15),
      child: Text(
        title,
        style: TextStyle(
          color: OnColors.black,
          fontSize: 16,
          fontWeight: FontWeight.normal,
        )
      ),
    );
  }

  _onCard (BuildContext context, val) => new Card(
    semanticContainer: true,
    clipBehavior: Clip.antiAliasWithSaveLayer,
    color: Colors.white,
    elevation: 0.0,
    margin: EdgeInsets.only(bottom: 15, top: 0, left: 0, right: 0),
    child: new InkWell(
      onTap: () {
        Navigator.of(context).pushNamed('chats-history');
      },
      onLongPress: () {
        _onMore(context);
      },
      child: new Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[

                new Container(
                  margin: EdgeInsets.only(right: 10),
                  width: 50,
                  height: 50,
                  color: OnColors.whiteGrey,
                  child: ClipRRect(
                    borderRadius: new BorderRadius.circular(10),
                    child: Image.asset('assets/post/2.jpg', fit: BoxFit.cover),
                  ),
                ),

                new Expanded(
                  child: new Container (
                    width: double.infinity,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Text(
                            'Green Tea ' + val.toString(),
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey.shade800,
                            ),
                            softWrap: true,
                        ),
                        new SizedBox(height: 5),

                        // new Row(
                        //   mainAxisAlignment: MainAxisAlignment.start,
                        //   crossAxisAlignment: CrossAxisAlignment.center,
                        //   children: <Widget>[
                        //     _onStart(3),
                        //     new SizedBox(width: 10),
                        //     new Text(
                        //       'Cabai rawit',
                        //       style: TextStyle(
                        //         fontSize: 14,
                        //         fontWeight: FontWeight.normal,
                        //         color: OnColors.lightGrey,
                        //       ),
                        //     ),
                        //     new SizedBox(width: 10),
                        //     new Container(
                        //       width: 40,
                        //       height: 20,
                        //       decoration: BoxDecoration(
                        //         borderRadius: BorderRadius.circular(20),
                        //         color: OnColors.secondary
                        //       ),
                        //       child: new Center(
                        //         child: new Text(
                        //           'Baru',
                        //           style: TextStyle(
                        //             color: Colors.white,
                        //             fontSize: 12,
                        //           )
                        //         )
                        //       ),
                        //     )
                        //   ],
                        // ),

                        // new SizedBox(height: 10),

                        new Text(
                          'Lorem ipsum, dolor sit amet consectetur adipisicing elit..',
                          style: TextStyle(color: OnColors.black, fontSize: 16),
                        ),

                        new SizedBox(height: 5),

                        new Text(
                          '5 menit yang lalu',
                          style: TextStyle(color: OnColors.lightGrey, fontSize: 16),
                        ),

                        // _onInfo (Icons.pin_drop, 'Lembang, Bandung Barat', Colors.grey.shade800),
                      ]
                    )
                  ),
                )

              ],
            ),

          ],
        )
      ),
    ),
  );

  // _onStart (var value) {
  //   return Container(
  //     child: new Row(
  //       mainAxisAlignment: MainAxisAlignment.start,
  //       children: <Widget>[
  //         new Icon(
  //           Icons.star,
  //           color: Colors.yellow,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.yellow,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.yellow,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.grey.shade300,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.grey.shade300,
  //           size: 16,
  //         ),
  //         new SizedBox(width: 5),
  //         new Text(
  //           '4.6',
  //           style: TextStyle(
  //             fontSize: 14,
  //             fontWeight: FontWeight.normal,
  //             color: Colors.grey.shade500,
  //           ),
  //           softWrap: true
  //         ),
  //       ],
  //     )
  //   );
  // }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Obrolan',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.search_plus,
              type: 'white',
              title: 'Lihat detail obrolan'
            ),
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.trash,
              type: 'white',
              title: 'Hapus obrolan'
            ),
            new SizedBox(height: 15),
            OnLine(),
            new SizedBox(height: 15),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

  // _onInfo (var icon, var value, var valColor) {
  //   return Container(
  //     margin: EdgeInsets.only(bottom: 2.5),
  //     child: Row(
  //       children: <Widget>[

  //         Container(
  //           margin: EdgeInsets.only(right: 2.5),
  //           child: Icon(
  //                 icon,
  //                 color: OnColors.main,
  //                 size: 18,
  //               ),
  //         ),

  //         Container(
  //           child: Text(
  //                 value,
  //                 style: TextStyle(
  //                   color: valColor,
  //                   fontWeight: FontWeight.normal,
  //                   fontSize: 14
  //                 ),
  //               ),
  //         )

  //       ],
  //     ),
  //   );
  // }
}