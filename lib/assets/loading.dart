import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Dialog(
        backgroundColor: Colors.transparent,
        elevation: 0,
        // shape: RoundedRectangleBorder(
        //   borderRadius: BorderRadius.circular(10),
        // ),
        child: new SizedBox(
          child: new Center(
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new CircularProgressIndicator(),
                new SizedBox(height: 15),
                new Text(
                  "Mohon tunggu..",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                  ),
                )
              ],
            )
          ),
        ),
      ),
    );
  }

}
