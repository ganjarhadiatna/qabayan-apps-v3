class BannerList {
  final List<Banner> banners;
  BannerList({
    this.banners
  });
  factory BannerList.fromJson(List<dynamic> parsedJson) {
    List<Banner> banners = new List<Banner>();
    banners = parsedJson.map((i) => Banner.fromJson(i)).toList();
    return new BannerList(
      banners: banners
    );
  }
}

class Banner {
  final String id;
  final String title;
  final String subtitle;
  final String image;
  Banner({
    this.id,
    this.title,
    this.subtitle,
    this.image
  });
  factory Banner.fromJson(Map<String, dynamic> parsedJson) {
    return Banner(
      id: parsedJson['id'].toString(),
      title: parsedJson['title'],
      subtitle: parsedJson['subtitle'],
      image: parsedJson['image']
    );
  }
}