import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
// import 'package:qabayan_ui/assets/onColors.dart';

class CardMethod extends StatelessWidget {

  final title;
  final subtitle;
  final icon;

  CardMethod({
    Key key,
    this.title,
    this.subtitle,
    this.icon
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
      margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        color: Colors.white,
      ),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[

          new Container(
              margin: EdgeInsets.only(right: 10),
              width: 45,
              height: 45,
              color: Colors.grey.shade100,
              child: ClipRRect(
                borderRadius: new BorderRadius.circular(45),
              ),
          ),

          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  this.title,
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontSize: 16,
                    fontWeight: FontWeight.bold
                  ),
                ),
                new SizedBox(height: 5),
                new Text(
                  this.subtitle,
                  style: TextStyle(
                    color: Colors.grey.shade600,
                    fontSize: 14,
                    fontWeight: FontWeight.normal
                  ),
                ),
              ],
            ),
          ),

          ButtonCircle(
            size: 22.0,
            type: 'white',
            icon: this.icon,
            action: () {},
          ),

        ],
      ),
    );
  }
}