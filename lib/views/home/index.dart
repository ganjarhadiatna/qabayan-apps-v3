import 'dart:async';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
import 'package:qabayan_ui/assets/titleContent.dart';
import 'package:qabayan_ui/views/posts/cardSlider.dart';
import 'package:qabayan_ui/views/explore/index.dart';
import 'package:qabayan_ui/services/item.dart';
import 'package:qabayan_ui/services/category.dart';

// import 'package:qabayan_ui/modules/Session.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List dataItem, dataCategory;
  String _id;

  Future getDataItem() async {
    var dataItem = await loadItem();
    setState(() {
      this.dataItem = dataItem;
    });
  }

  Future getDataCategory() async {
    var dataCategory = await loadCategory();
    setState(() {
      this.dataCategory = dataCategory;
    });
  }

  Future getUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString('id');
    setState(() {
      this._id = id;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getDataItem();
    this.getDataCategory();
    this.getUserID();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            new SizedBox(width: 20),

            new Expanded(
              child: new Container (
                padding: EdgeInsets.only(top: 17),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      'Qabayan',
                      style: new TextStyle(
                        color: OnColors.main,
                        fontWeight: FontWeight.bold,
                        fontSize: 28
                      ),
                    )
                  ],
                ),
              ),
            ),

            new Container(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  new Container(
                    margin: EdgeInsets.only(top: 15, right: 5),
                    child: _onSearch(),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 12.5),
                    child: ButtonCircle(
                      action: () {
                        Navigator.of(context).pushNamed('cart');
                      },
                      icon: LineIcons.shopping_cart,
                      size: 32.0,
                      type: 'white',
                    )
                  )
                ]
              )
            ),

            new SizedBox(width: 10)
          ],
        ),
      ),
      body: new ListView(
        children: <Widget>[
          _onFeature(),
          new SizedBox(height: 20),
          _onSocial(),
          new SizedBox(height: 20),
          new Container(
            child: (this._id == null) 
            ? new Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 20), 
              child: _onFormLogin()
            ) 
            : null,
          ),
          _onCategory(),
          new SizedBox(height: 20),
          _onItem('Sayuran hari ini', () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ExplorePage(
                  index: 0,
                  title: 'Sayuran Hari Ini'
                )
              )
            );
          }),
          new SizedBox(height: 20),
          _onItem('Paling banyak dicari', () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ExplorePage(
                  index: 0,
                  title: 'Paling Banyak Dicari'
                )
              )
            );
          }),
          new SizedBox(height: 20),
          _onHelp(),
          new SizedBox(height: 20)
        ],
      ),
    );
  }

  // components
  _onSearch () {
    return new InkWell(
      onTap: () {
        Navigator.of(context).pushNamed('search');
      },
      child: new Container(
        width: 130,
        height: 40,
        padding: EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          color: Colors.white,
          border: Border.all(color: Colors.grey.shade300, style: BorderStyle.solid),
        ),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Container(
              margin: EdgeInsets.only(right: 5),
              child: new Icon(
                LineIcons.search,
                color: OnColors.grey,
                size: 20
              ),
            ),
            new Container(
              child: new Text(
                'Cari Sayuran',
                style: TextStyle(
                  color: OnColors.grey,
                  fontWeight: FontWeight.normal,
                  fontSize: 14
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _onHelp () {
    return new Card(
      elevation: 2.5,
      color: Colors.white,
      margin: EdgeInsets.only(left: 15, right: 15),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: new Container(
        // color: Colors.white,
        padding: EdgeInsets.all(15),
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TitleContent(
              title: 'Perlu bantuan?',
              subtitle: 'kami ada 24 jam, siap membantu kamu.',
              more: '',
              action: () {},
            ),
            new SizedBox(height: 10),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                new Container(
                  width: 100,
                  child: ButtonText(
                    title: 'TELPON',
                    type: 'main-reverse',
                    action: () {},
                  ),
                ),
                new Container(
                  width: 100,
                  child: ButtonText(
                    title: 'WA',
                    type: 'main-reverse',
                    action: () {},
                  ),
                ),
                new Container(
                  width: 100,
                  child: ButtonText(
                    title: 'EMAIL',
                    type: 'main-reverse',
                    action: () {},
                  ),
                )
              ],
            )

          ],
        ),
      ),
    );
  }

  _onFeature () {
    return new Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
      child: new Card(
        color: OnColors.main,
        elevation: 0,
        margin: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)
        ),
        child: new Container(
          padding: EdgeInsets.all(0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                width: double.infinity,
                padding: EdgeInsets.all(15),
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                ),
                child: new Text(
                  'Meja jualan',
                  style: new TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold
                  )
                ),
              ),
              new Container(
                padding: EdgeInsets.all(10),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Expanded(
                      child: _onCardFeature('Tambah Sayuran', LineIcons.plus_square_o, () {
                        (this._id == null) ? Navigator.of(context).pushNamed('login') : Navigator.of(context).pushNamed('compose');
                      }),
                    ),
                    new SizedBox(width: 10),
                    new Expanded(
                      child: _onCardFeature('Sedang Dijual', LineIcons.clock_o, () {
                        (this._id == null) ? Navigator.of(context).pushNamed('login') : Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ExplorePage(
                              index: 0,
                              title: 'Sedang Dijual'
                            )
                          )
                        );
                      }),
                    ),
                    new SizedBox(width: 10),
                    new Expanded(
                      child: _onCardFeature('Paling Laku', LineIcons.arrow_circle_o_up, () {
                        (this._id == null) ? Navigator.of(context).pushNamed('login') : Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ExplorePage(
                              index: 0,
                              title: 'Paling Laku'
                            )
                          )
                        );
                      }),
                    ),
                    new SizedBox(width: 10),
                    new Expanded(
                      child: _onCardFeature('Belum Dipublikasi', LineIcons.th_list, () {
                        (this._id == null) ? Navigator.of(context).pushNamed('login') : Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ExplorePage(
                              index: 0,
                              title: 'Belum Dipublikasi'
                            )
                          )
                        );
                      })
                    )
                  ]
                )
              )
            ]
          )
        )
      )
    );
  }

  _onFormLogin () {
    return new Card(
      color: OnColors.whiteGrey,
      elevation: 0,
      margin: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10)
      ),
      child: new Container(
        padding: EdgeInsets.all(12.5),
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Expanded(
              child: new Text(
                'Masuk ke akun kamu dan gunakan semua fitur di aplikasi ini.',
                style: TextStyle(
                  color: OnColors.black,
                  fontSize: 15,
                ),
              ),
            ),
            new SizedBox(width: 10),
            new Container(
              width: 100,
              child: ButtonText(
                action: () {
                  Navigator.of(context).pushNamed('login');
                },
                type: 'main',
                title: 'Login',
              ),
            )
          ],
        )
      ),
    );
  }

  _onSlideCategory (var val) {
    return new Container(
      height: 80,
      child: new ListView(
        padding: EdgeInsets.only(left: 15),
        scrollDirection: Axis.horizontal,
        children: List.generate(val, (index) {
          return new Column(
            children: <Widget>[
              // new SizedBox(height: 2),
              // new Container(
              //   margin: EdgeInsets.only(right: 15),
              //   child: this._onCardCategory(
              //     this.dataCategory[index].title, 
              //     this.dataCategory[index].image,
              //     this.dataCategory[index].totalItem
              //   ),
              // ),
              // new SizedBox(height: 15),
              new Container(
                margin: EdgeInsets.only(right: 15),
                child: this._onCardCategorySmall(
                  this.dataCategory[index].title, 
                  this.dataCategory[index].image,
                  this.dataCategory[index].totalItem
                ),
              )
            ],
          );
        })
      ),
    );
  }

  _onSlideItem (var val) {
    return new Container(
      height: 240,
      child: new ListView(
        padding: EdgeInsets.only(left: 15),
        scrollDirection: Axis.horizontal,
        children: List.generate(val, (index) {
          return CardSlider(
            title: this.dataItem[index].title,
            image: this.dataItem[index].image,
            price: this.dataItem[index].price,
            status: this.dataItem[index].status
          );
        })
      ),
    );
  }

  _onFullCategory (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Kategori sayuran',
        buttonClose: true,
        content: new Column(
          children: <Widget>[
            new Container(
              width: double.infinity,
              height: 500,
              child: new GridView.count(
                scrollDirection: Axis.vertical,
                crossAxisCount: 3,
                shrinkWrap: true,
                childAspectRatio: 0.8,
                padding: EdgeInsets.only(top: 10),
                children: List.generate(this.dataCategory.length, (index) {
                  return new Container(
                    child: this._onCardCategory(
                      this.dataCategory[index].title, 
                      this.dataCategory[index].image,
                      this.dataCategory[index].totalItem
                    ),
                  );
                })
              )
            ),
          ]
        ),
      ),
    );
  }

  _onItem(var title, var action) {
    return new Container(
      color: Colors.white,
      // padding: EdgeInsets.only(top: 15, bottom:15),
      child: new Column(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(left: 15, right: 15),
            child: TitleContent(
              title: title,
              subtitle: '',
              more: 'Lainnya',
              action: action,
            ),
          ),
          new Container(
            margin: EdgeInsets.only(top: 15, bottom: 0),
            child: _onSlideItem(5),
          ),
        ]
      )
    );
  }

  _onCategory() {
    return new Container(
      color: Colors.white,
      // padding: EdgeInsets.only(top: 15, bottom:15),
      child: new Column(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(left: 15, right: 15),
            child: TitleContent(
              title: 'Kategori sayuran',
              subtitle: '',
              more: 'Lainnya',
              action: () {
                _onFullCategory(context);
              },
            ),
          ),
          new Container(
            margin: EdgeInsets.only(top: 15),
            child: _onSlideCategory(5),
          ),
        ],
      )
    );
  }

  _onSocial() {
    return new Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      child: new Card(
        color: Colors.white,
        elevation: 2.5,
        margin: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)
        ),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Expanded(child: _onCardSocial(Icons.compare_arrows, 'Negosiasi Harga', () {
              Navigator.of(context).pushNamed('bergains');
            }, false)),
            new Expanded(child: _onCardSocial(LineIcons.comments, 'Ngobrol Sayuran', () {
              Navigator.of(context).pushNamed('chats');
            }, true)),
            // new Expanded(child: _onCardSocial(LineIcons.list_ul, 'Transaksi', () {
            //   Navigator.of(context).pushNamed('chats');
            // }, true)),
          ],
        ),
      )
    );
  }

  // cards
  _onCardSocial(var icon, var title, var action, bool border) {
    return new InkWell(
      onTap: action,
      child: new Container(
        width: double.infinity,
        padding: EdgeInsets.all(10),
        decoration: (border) ? BoxDecoration(
          border: Border(left: BorderSide(color: Colors.grey.shade300, width: 1))
        ) : null,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              width: 40,
              height: 40,
              child: new Center(
                child: new Icon(
                  icon,
                  color: OnColors.main,
                  size: 24,
                ),
              ),
            ),
            new Expanded(
              child: new Text(
                title,
                style: new TextStyle(
                  color: OnColors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 14
                ),
              )
            )
          ],
        ),
      ),
    );
  }
  _onCardCategorySmall(var title, var image, var totalItem) {
    return new Card(
      color: Colors.white,
      elevation: 2.5,
      margin: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10)
      ),
      child: new InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => ExplorePage(
                index: 0,
                title: title
              )
            )
          );
        },
        child: new Container(
          width: 183,
          height: 70,
          padding: EdgeInsets.all(10),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(
                width: 45,
                height: 45,
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.circular(45),
                  color: OnColors.whiteGrey
                ),
                child: ClipRRect(
                  borderRadius: new BorderRadius.circular(45),
                  child: Image.asset(
                    image,
                    fit: BoxFit.cover,
                  ),
                )
              ),
              new SizedBox(width: 10),
              new Expanded(
                child: new Text(
                  title,
                  style: TextStyle(
                    color: OnColors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.normal
                  ),
                )
              )
            ],
          ),
        ),
      )
    );
  }

  _onCardCategory(var title, var image, var totalItem) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => ExplorePage(
                    index: 0,
                    title: title
                  )
                )
              );
            },
            child: new Container(
              width: 111,
              height: 111,
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: OnColors.whiteGrey
              ),
              child: ClipRRect(
                borderRadius: new BorderRadius.circular(10),
                child: Image.asset(
                  image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          new SizedBox(height: 5),
          new Text(
            title,
            style: TextStyle(
              color: OnColors.black,
              fontSize: 16,
              fontWeight: FontWeight.normal
            ),
          ),
        ],
      )
    );
  }

  _onCardFeature (var title, var icon, var action) {
    return new InkWell(
      onTap: action,
      child: new Container(
        height: 90,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              width: 50,
              height: 50,
              child: new Center(
                child: new Icon(
                  icon,
                  size: 32,
                  color: Colors.white
                ),
              )
            ),
            // new SizedBox(height: 10),
            new Container(
              width: 65,
              child: new Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}