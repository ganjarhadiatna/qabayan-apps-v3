import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
import 'package:qabayan_ui/assets/popupLoading.dart';
import 'package:qabayan_ui/assets/popupQuestion.dart';

class CardCart extends StatelessWidget {
  final type;
  final value;

  CardCart({
    Key key,
    @required this.type,
    @required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      child: new Column(
        children: <Widget>[
          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _onCheck(),
                new InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed('user-profile');
                  },
                  child: new Container(
                    margin: EdgeInsets.only(right: 10),
                    width: 35,
                    height: 35,
                    color: OnColors.whiteGrey,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(5),
                      // child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                    ),
                  ),
                ),
                new Container(
                  child: new Text(
                    'Green Tea',
                    style: TextStyle(
                      color: OnColors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.normal
                    )
                  ),
                ),
                new Expanded(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      ButtonCircle(
                        action: () {
                          Navigator.of(context).pushNamed('chats-history');
                        },
                        icon: LineIcons.comments,
                        size: 22.0,
                        type: "white",
                      ),
                      ButtonCircle(
                        action: () {
                          Navigator.of(context).pushNamed('user-profile');
                        },
                        icon: Icons.chevron_right,
                        size: 28.0,
                        type: 'white',
                      )
                    ],
                  ),
                )
              ],
            ),
          ),

          OnLine(),

          // item
          _onItem(context, this.type),

        ],
      )
    );
  }

  _onItem (BuildContext context, type) {
    var _tools;
    var _bottom;
    if (type == 'kiloan') {
      _bottom = new Container();
      _tools = new Container(
        width: double.maxFinite,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Expanded(
              child: _onPlusMinus(),
            ),
            _onSmallButton(LineIcons.trash, () {
              _onDeleteCard(context);
            })
          ],
        ),
      );
    } else {
      _tools = new Container();
      _bottom = new Container(
        width: double.maxFinite,
        padding: EdgeInsets.only(bottom: 5),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              width: 160,
              child: ButtonText(
                action: () {},
                title: 'Pesan Barang',
                type: 'grey',
              ),
            ),
            _onSmallButton(LineIcons.trash, () {
              _onDeleteCard(context);
            })
          ],
        ),
      );
    }
    
    // card barang
    return new Container(
      child: new Container(
          padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          color: Colors.white,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _onCheck(),
              new Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      // margin: EdgeInsets.only(bottom: 5),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[

                          new Expanded(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Text(
                                  'Cabai rawit merah',
                                  style: TextStyle(
                                    color: Colors.grey.shade800,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                new SizedBox(height: 5),
                                _onInfo ('Stok', '2 barang', Colors.grey.shade800),
                                _onInfo ('Harga', 'IDR 45 K', OnColors.tertiary),
                                new SizedBox(height: 10),
                                // _cardNotes('Jumlah barang yang ingin dipesan.'),
                                // new SizedBox(height: 5),
                                
                              ],
                            ),
                          ),

                          new InkWell(
                            onTap: () {
                              return _onCardDialog(context);
                            },
                            child: new Container(
                              margin: EdgeInsets.only(left: 15),
                              width: 70,
                              height: 70,
                              color: OnColors.whiteGrey,
                              child: ClipRRect(
                                borderRadius: new BorderRadius.circular(5),
                                // child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                              ),
                            ),
                          ),

                        ]
                      ),
                    ),

                    _tools,
                    _bottom
                  ]
                )
              )
            ]
          )
        )

    );
  }

  _onPlusMinus () {
    return new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // _cardNotes('Jumlah barang yang ingin dipesan.'),
            new Container(
              // margin: EdgeInsets.only(top: 10, bottom: 10),
              width: 150,
              height: 30,
              decoration: BoxDecoration(
                color: OnColors.whiteGrey,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                  color: Colors.grey.shade300
                ),
              ),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                  _onSmallButton(LineIcons.minus, () {}),

                  // new SizedBox(width: 10),

                  new Expanded(
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      autofocus: false,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        fillColor: OnColors.whiteGrey,
                        hintText: '0',
                        // border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        border:  OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0),
                          borderSide: BorderSide.none,
                        ),
                      ),
                    ),
                  ),

                  // new SizedBox(width: 10),

                  _onSmallButton(LineIcons.plus, () {}),

                ],
              ),
            ),
          ],

    );
  }

  _onCardDialog (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Barang',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              action: () {
                Navigator.of(context).pushNamed('detail-post');
              },
              type: 'white',
              icon: LineIcons.search_plus,
              title: 'Lihat detail barang'
            ),
            ButtonTextIcon(
              action: () {
                _onDeleteCard(context);
              },
              type: 'white',
              icon: LineIcons.trash,
              title: 'Hapus barang dari keranjang'
            ),

            new Container(
              margin: EdgeInsets.only(top: 15, bottom: 15),
              child: OnLine(),
            ),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

  _onSmallButton (var icon, var action) {
    return new Container(
      // borderRadius: BorderRadius.all(Radius.circular(0)),
      width: 35,
      child: new MaterialButton(
        // minWidth: 35,
        height: 30,
        padding: EdgeInsets.only(left: 0, right: 0),
        color: Colors.white,
        elevation: 0,
        onPressed: action,
        child: new Center(
          child: Icon(
            icon,
            size: 22,
            color: OnColors.grey,
          ),
        ),
      )
    );
  }

  _onCheck () {
    return new Container(
      margin: EdgeInsets.only(right: 15),
      width: 25,
      height: 25,
      decoration: BoxDecoration(
        color: OnColors.main,
        borderRadius: BorderRadius.circular(5)
      ),
      child: new Center(
        child: new Icon(
          LineIcons.check,
          size: 16,
          color: Colors.white,
        ),
      ),
    );
  }

  _onDeleteCard (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupQuestion(
        title: 'Konfirmasi',
        subtitle: 'Hapus barang dari keranjang?',
        action: () {
          Navigator.pop(context);
          showDialog(
            context: context,
            builder: (BuildContext context) => PopupLoading(title: 'Menghapus barang dari keranjang..')
          );
        }
      )
    );
  }

  _onInfo (var title, var value, var valColor) {
    return Container(
      margin: EdgeInsets.only(bottom: 2.5, top: 2.5),
      child: Row(
        children: <Widget>[

          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontWeight: FontWeight.normal,
                    fontSize: 16
                  ),
                )
              ],
            ),
          ),

          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  value,
                  style: TextStyle(
                    color: valColor,
                    fontWeight: FontWeight.normal,
                    fontSize: 16
                  ),
                )
              ],
            ),
          ),

        ],
      ),
    );
  }

  // _cardMainTitile (var title) {
  //   return Text(
  //     title, 
  //     style: TextStyle(
  //       fontWeight: FontWeight.bold,
  //       fontSize: 16.0,
  //       color: Colors.grey.shade800
  //     ),
  //     softWrap: true
  //   );
  // }

  // _cardNotes (var title) {
  //   return Text(
  //     title,
  //     style: TextStyle(
  //       color: Colors.grey.shade600,
  //       fontWeight: FontWeight.normal,
  //       fontSize: 14.0,
  //     ),
  //   );
  // }
}