import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/views/orders/cardMethod.dart';

class TypeComposePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Tipe Penjualan'),

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new Container(
        color: Colors.grey.shade100,
        child: new ListView(
          padding: EdgeInsets.only(top: 15),
          children: <Widget>[
            _onTitle('Pilih satu tipe penjualan'),
            CardMethod(
              title: 'Kiloan', 
              subtitle: 'Penjualan sayur secara kiloan',
              icon: Icons.check),
            CardMethod(
              title: 'Borongan', 
              subtitle: 'Penjualan sayur secara borongan',
              icon: Icons.check),
          ],
        ),
      ),

      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        elevation: 0.0,
        clipBehavior: Clip.none,
        child: new Container(
          width: double.infinity,
          padding: EdgeInsets.all(15),
          child: ButtonText(
            title: 'Simpan Perubahan',
            type: 'main',
            action: () {
              Navigator.pop(context);
            },
          ),
        )
      )

    );
  }

  _onTitle (var title) {
    return Container (
      margin: EdgeInsets.fromLTRB(20, 0, 20, 15),
      child: Text(
        title,
        style: TextStyle(
          color: OnColors.black,
          fontSize: 16,
          fontWeight: FontWeight.normal,
        )
      ),
    );
  }

}