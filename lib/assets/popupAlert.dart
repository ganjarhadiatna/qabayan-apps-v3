import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/buttonText.dart';

class PopupAlert extends StatelessWidget {

  final title;
  final subtitle;
  final action;

  PopupAlert({
    Key key,
    @required this.title,
    @required this.subtitle,
    @required this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
      alignment: Alignment(0,0),
      child: new Container(
        width: 320,
        padding: EdgeInsets.all(20),
        margin: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15)
        ),
        child: new Material(
          color: Colors.transparent,
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              new Text(
                this.title,
                style: TextStyle(
                  color: Colors.grey.shade800,
                  fontSize: 24,
                  fontWeight: FontWeight.bold
                ),
              ),

              SizedBox(height: 5),

              new Text(
                this.subtitle,
                style: TextStyle(
                  color: Colors.grey.shade800,
                  fontSize: 16,
                  fontWeight: FontWeight.normal
                ),
              ),

              SizedBox(height: 15),

              new Container(
                child: ButtonText(
                  action: this.action,
                  title: 'Ok, Saya Mengerti',
                  type: 'main',
                )
              )

            ],
          ),
        ),
      ),
    );
  }
}