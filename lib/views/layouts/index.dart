import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/views/home/index.dart';
import 'package:qabayan_ui/views/transactions/index.dart';
import 'package:qabayan_ui/views/inbox/index.dart';
import 'package:qabayan_ui/views/account/index.dart';

class Layout extends StatefulWidget {
  @override
  _LayoutState createState() => _LayoutState();
}

class _LayoutState extends State<Layout> {
  int _selectedIndex = 0;
  String _id;

  final _widgetOptions = [
    HomePage(),
    TransactionsPage(),
    InboxPage(),
    AccountPage()
  ];

  final _widgetColor = [
    OnColors.main,
    OnColors.black,
    OnColors.black,
    OnColors.black,
  ];

  final _widgetColorSelected = [
    OnColors.main,
    OnColors.main,
    OnColors.main,
    OnColors.main,
  ];

  Future getUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString('id');
    setState(() {
      this._id = id;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getUserID();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: new BottomAppBar(
        color: Colors.white,
        elevation: 0,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _onButtonBottom(
              LineIcons.home, 
              'Home', 
              _widgetColor.elementAt(0), 
              28.0, 
              0),
            _onButtonBottom(
              LineIcons.list, 
              'Transaksi', 
              _widgetColor.elementAt(1), 
              28.0, 
              1),
            _onButtonBottom(
              LineIcons.bell_o, 
              'Notifikasi', 
              _widgetColor.elementAt(2), 
              28.0, 
              2),
            _onButtonBottom(
              LineIcons.user, 
              'Akun', 
              _widgetColor.elementAt(3), 
              28.0, 
              3),
          ]
        )
      )
    );
  }

  _onButtonBottom (var icon, var title, var color, var size, var index) {
    return new Expanded(
      child: new InkWell(
        onTap: () {
          (index == 0) 
          ? _onItemTapped(index) 
          : (this._id == null) 
            ? Navigator.of(context).pushNamed('login') 
            : _onItemTapped(index);
        },
        child: new Container(
          width: double.infinity,
          height: 70,
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Icon(
                icon, 
                size: size, 
                color: color,
              ),
              new Text(
                title, 
                style: TextStyle(
                  color: color, 
                  fontSize: 14
                )
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onItemTapped(int index) {

    _widgetColor[0] = OnColors.black;
    _widgetColor[1] = OnColors.black;
    _widgetColor[2] = OnColors.black;
    _widgetColor[3] = OnColors.black;

    setState(() {

      _widgetColor[index] = _widgetColorSelected[index];
      _selectedIndex = index;

    });
  }
}