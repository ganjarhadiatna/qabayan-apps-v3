import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class CardListWidget extends StatelessWidget {
  final String image;
  final String title;
  final String price;
  final String secondPrice;
  final String status;
  final String category;
  final String location;

  CardListWidget({
    Key key,
    this.image,
    this.title,
    this.price,
    this.secondPrice,
    this.status,
    this.category,
    this.location
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _onCard(context);
  }

  _onCard (BuildContext context) => new Card(
    semanticContainer: true,
    clipBehavior: Clip.antiAliasWithSaveLayer,
    color: Colors.white,
    elevation: 0.0,
    margin: EdgeInsets.only(bottom: 15, top: 0, left: 0, right: 0),
    child: new InkWell(
      onTap: () {
        Navigator.of(context).pushNamed('detail-post');
      },
      child: new Container(
        padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[

                new Container(
                  margin: EdgeInsets.only(right: 10),
                  width: 75,
                  height: 75,
                  color: OnColors.whiteGrey,
                  child: ClipRRect(
                    borderRadius: new BorderRadius.circular(10),
                    child: Image.asset(
                      this.image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),

                new Expanded(
                  child: new Container (
                    width: double.infinity,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Text(
                            this.title,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: OnColors.black
                            ),
                            softWrap: true,
                        ),
                        new SizedBox(height: 5),

                        new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            // _onStart(3),
                            // new SizedBox(width: 10),
                            new Text(
                              this.category,
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.normal,
                                color: OnColors.lightGrey,
                              ),
                            ),
                            new SizedBox(width: 10),
                            new Container(
                              // width: 40,
                              padding: EdgeInsets.only(left: 10, right: 10),
                              height: 20,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: OnColors.secondary
                              ),
                              child: new Center(
                                child: new Text(
                                  this.status,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                  )
                                )
                              ),
                            )
                          ],
                        ),

                        new SizedBox(height: 10),

                        _onInfo (Icons.pin_drop, this.location, Colors.grey.shade800),
                      ]
                    )
                  ),
                )

              ],
            ),

            // new SizedBox(height: 15),
            // new OnLine(),
            new SizedBox(height: 10),

            new Container(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    textDirection: TextDirection.ltr,
                    children: <Widget>[
                      
                      new Text(
                        'Harga sayuran',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: OnColors.black
                        ),
                        softWrap: true
                      ),

                      new Text(
                        'per kilogram',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.normal,
                          color: OnColors.grey
                        ),
                        softWrap: true
                      ),

                    ],
                  ),

                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    textDirection: TextDirection.ltr,
                    children: <Widget>[
                      new Text(
                        this.price,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: Colors.grey.shade500,
                          decoration: TextDecoration.lineThrough
                        ),
                        softWrap: true
                      ),
                      
                      new Text(
                        this.secondPrice,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: OnColors.tertiary
                        ),
                        softWrap: true
                      ),
                    ],
                  ),

                ],
                
              )
            ),

            new SizedBox(height: 15),
            new OnLine(),
            new SizedBox(height: 15),

            new Container(
              // padding: EdgeInsets.all(10.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                  new InkWell(
                      onTap: () {
                        Navigator.of(context).pushNamed('chats-history');
                      },
                      child: new Container(
                        width: 80,
                        height: 45,
                        decoration: BoxDecoration(
                          color: Colors.transparent
                        ),
                        child: new Center(
                          // padding: EdgeInsets.only(top: 7, bottom: 7),
                          child: new Icon(
                            LineIcons.comments,
                            color: OnColors.black,
                            size: 28,
                          ),
                        )
                      ),

                  ),

                  new Container(
                    width: 0.5,
                    height: 30,
                    color: OnColors.grey
                  ),

                  new InkWell(
                      onTap: () {},
                      child: new Container(
                        width: 80,
                        height: 45,
                        decoration: BoxDecoration(
                          color: Colors.transparent
                        ),
                        child: new Center(
                          // padding: EdgeInsets.only(top: 7, bottom: 7),
                          child: new Icon(
                            LineIcons.cart_plus,
                            color: OnColors.black,
                            size: 28,
                          ),
                        )
                      )

                  ),

                  new Container(
                    width: 0.5,
                    height: 30,
                    color: OnColors.grey
                  ),

                  new SizedBox(width: 20),

                  new Expanded(
                    child: ButtonText(
                      action: () {
                        Navigator.of(context).pushNamed('make-order');
                      },
                      title: 'Beli Sayuran',
                      type: 'main',
                    )
                  )

                ],
              )
            )
          ],
        )
      ),
    ),
  );

  // _onStart (var value) {
  //   return Container(
  //     child: new Row(
  //       mainAxisAlignment: MainAxisAlignment.start,
  //       children: <Widget>[
  //         new Icon(
  //           Icons.star,
  //           color: Colors.yellow,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.yellow,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.yellow,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.grey.shade300,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.grey.shade300,
  //           size: 16,
  //         ),
  //         new SizedBox(width: 5),
  //         new Text(
  //           '4.6',
  //           style: TextStyle(
  //             fontSize: 14,
  //             fontWeight: FontWeight.normal,
  //             color: Colors.grey.shade500,
  //           ),
  //           softWrap: true
  //         ),
  //       ],
  //     )
  //   );
  // }

  _onInfo (var icon, var value, var valColor) {
    return Container(
      margin: EdgeInsets.only(bottom: 2.5),
      child: Row(
        children: <Widget>[

          Container(
            margin: EdgeInsets.only(right: 2.5),
            child: Icon(
              icon,
              color: OnColors.main,
              size: 18,
            ),
          ),

          Container(
            child: Text(
              value,
              style: TextStyle(
                color: valColor,
                fontWeight: FontWeight.normal,
                fontSize: 14
              ),
            ),
          )

        ],
      ),
    );
  }
}