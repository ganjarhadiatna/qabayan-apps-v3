import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class ButtonCircle extends StatelessWidget {
  final icon;
  final type;
  final size;
  final action;

  ButtonCircle({
    Key key,
    @required this.icon,
    @required this.size,
    @required this.type,
    @required this.action
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var buttonColor;
    var textColor;

    if (this.type == 'main') {
      buttonColor = OnColors.main;
      textColor = Colors.white;
    }

    if (this.type == 'primary') {
      buttonColor = OnColors.primary;
      textColor = OnColors.black;
    }

    if (this.type == 'secondary') {
      buttonColor = OnColors.secondary;
      textColor = Colors.white;
    }

    if (this.type == 'grey') {
      buttonColor = OnColors.whiteGrey;
      textColor = OnColors.black;
    }

    if (this.type == 'white') {
      buttonColor = Colors.white;
      textColor = OnColors.black;
    }
    


    return new ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(45)),
      child: new MaterialButton(
        // borderRadius: BorderRadius.circular(35.0),
        minWidth: 45,
        height: 45,
        padding: EdgeInsets.only(left: 0, right: 0),
        color: buttonColor,
        elevation: 0,
        onPressed: this.action,
        child: new Center(
          child: Icon(
            this.icon,
            size: this.size,
            color: textColor,
          ),
        ),
      )
    );

  }
}