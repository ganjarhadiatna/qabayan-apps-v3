import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/views/transactions/orders/card.dart';
import 'package:qabayan_ui/views/transactions/sales/card.dart';

class TransactionsPage extends StatefulWidget {
  @override
  _TransactionsPageState createState() => _TransactionsPageState();
}

class _TransactionsPageState extends State<TransactionsPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            new SizedBox(width: 20),

            new Expanded(
              child: new Container (
                padding: EdgeInsets.only(top: 17),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      'Transaksi',
                      style: new TextStyle(
                        color: OnColors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 28
                      ),
                    )
                  ],
                ),
              ),
            ),

            new Container(
              padding: EdgeInsets.only(top: 12.5),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ButtonCircle(
                    action: () {},
                    icon: LineIcons.search,
                    size: 26.0,
                    type: 'white',
                  ),
                  ButtonCircle(
                    action: () {
                      Navigator.of(context).pushNamed('chats');
                    },
                    icon: LineIcons.comments_o,
                    size: 26.0,
                    type: 'white',
                  ),
                  ButtonCircle(
                    action: () {
                      Navigator.of(context).pushNamed('cart');
                    },
                    icon: LineIcons.shopping_cart,
                    size: 32.0,
                    type: 'white',
                  )
                ]
              )
            ),

            new SizedBox(width: 10)
          ],
        ),
      ),
      body: new DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: new Scaffold(
          backgroundColor: Colors.white,
          appBar: new PreferredSize(
            preferredSize: Size(double.infinity, 60),
            child: new Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: new TabBar(
                unselectedLabelColor: Colors.grey.shade600,
                labelColor: Colors.white,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: OnColors.main,
                  border: Border.all(
                    width: 1,
                    color: OnColors.main
                  )
                ),
                tabs: <Widget>[
                  new Tab(
                    child: new Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Pembelian'),
                      ),
                    ),
                  ),
                  new Tab(
                    child: new Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Penjualan'),
                      ),
                    ),
                  ),
                ]
              ),
            )
          ),
          body: new TabBarView(
            dragStartBehavior: DragStartBehavior.down,
            children: <Widget>[
              CardOrderWidget(),
              CardSalesWidget(),
            ],
          ),
        ),
      ),
    );
  }
}