import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/src/carousel_pro.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
// import 'package:qabayan_ui/views/posts/cardOffers.dart';

class DetailPostPage extends StatefulWidget {
  @override
  _DetailPostPageState createState() => _DetailPostPageState();
}

class _DetailPostPageState extends State<DetailPostPage> {
  @override
  Widget build(BuildContext context) {

    return new Scaffold(

      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Sayuran'),
                    
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          ButtonCircle(
                            size: 24.0,
                            type: 'white',
                            icon: LineIcons.search,
                            action: () {
                              Navigator.of(context).pushNamed('search');
                            }
                          ),
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: LineIcons.shopping_cart,
                            action: () {
                              Navigator.of(context).pushNamed('cart');
                            }
                          ),
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: Icons.more_vert,
                            action: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) => PopupBottom(
                                  title: 'Pengaturan Sayuran',
                                  content: new Column(
                                    children: <Widget>[
                                      ButtonTextIcon(
                                        action: () {},
                                        type: 'white',
                                        icon: LineIcons.pencil,
                                        title: 'Edit sayuran'
                                      ),
                                      ButtonTextIcon(
                                        action: () {},
                                        type: 'white',
                                        icon: LineIcons.trash,
                                        title: 'Hapus sayuran'
                                      ),

                                      new Container(
                                        margin: EdgeInsets.only(top: 15, bottom: 15),
                                        child: OnLine(),
                                      ),

                                      ButtonText(
                                        action: () {
                                          Navigator.pop(context);
                                        },
                                        type: 'grey',
                                        title: 'Tutup'
                                      ),
                                    ]
                                  ),
                                ),
                              );
                            },
                          ),

                        ],
                      ),
                    )

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new ListView(
        children: <Widget>[
          
          new Container(
            height: 300,
            child: new Carousel(
              images: [
                new ExactAssetImage('assets/post/1.jpg'),
                new ExactAssetImage('assets/post/2.jpg'),
                // new ExactAssetImage(''),
                // new ExactAssetImage(''),
              ],
              animationDuration: const Duration(seconds: 1),
              dotBgColor: Colors.transparent,
              autoplay: false,
              dotColor: Colors.white,
              dotSize: 5,
              indicatorBgPadding: 10,
            ),
          ),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            margin: EdgeInsets.only(bottom: 15),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                new Text(
                  'Cabai Rawit',
                  style: TextStyle(
                    color: OnColors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),

                new Container(height: 10),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    // _onStart(3),
                    // new SizedBox(width: 10),
                    new Text(
                      'Cabai rawit',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.normal,
                        color: OnColors.lightGrey,
                      ),
                    ),
                    new SizedBox(width: 10),
                    new Container(
                      width: 40,
                      height: 20,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: OnColors.secondary
                      ),
                      child: new Center(
                        child: new Text(
                          'Baru',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          )
                        )
                      ),
                    )
                  ],
                ),

                new Container(
                  margin: EdgeInsets.only(top: 10, bottom: 5),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                        child: _cardInfo (Icons.person, 'green tea'),
                      ),
                      new Container(
                        child: _cardInfo (Icons.pin_drop, 'Lembang, Bandung Barat'),
                      )
                    ],
                  )
                ),
                
                new Container(
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  child: OnLine(),
                ),
                
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      textDirection: TextDirection.ltr,
                      children: <Widget>[
                        new Text(
                          'Rp 17.000',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey.shade500,
                            decoration: TextDecoration.lineThrough
                          ),
                          softWrap: true
                        ),
                        
                        new Text(
                          'Rp 15.000',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: OnColors.tertiary
                          ),
                          softWrap: true
                        ),
                      ],
                    ),

                    new RaisedButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed('bergains-order');
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)
                      ),
                      elevation: 0,
                      color: Colors.grey.shade100,
                      disabledElevation: 0,
                      child: new Row(
                        children: <Widget>[
                          // Icon(Icons.attach_money, size: 18),
                          Text(
                              'Negosiasi harga?',
                              style: TextStyle(color: Colors.grey.shade800
                            )
                          ),
                        ],
                      )
                    )

                  ],
                )

              ]
            )
          ),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            margin: EdgeInsets.only(bottom: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                _cardTitile('Informasi'),

                new Container(height: 10),

                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Stok sayuran',
                      style: TextStyle(
                        color: Colors.grey.shade800,
                        fontWeight: FontWeight.normal,
                        fontSize: 15,
                      ),
                    ),
                    Text(
                      'Sisa 30 Kg',
                      style: TextStyle(
                        color: Colors.grey.shade800,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                  ]
                ),

                new Container(height: 5),

                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Tipe penjualan',
                      style: TextStyle(
                        color: Colors.grey.shade800,
                        fontWeight: FontWeight.normal,
                        fontSize: 15,
                      ),
                    ),
                    Text(
                      'Kiloan',
                      style: TextStyle(
                        color: Colors.grey.shade800,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                  ]
                ),

                new Container(
                  margin: EdgeInsets.only(top: 15, bottom: 15),
                  child: OnLine(),
                ),

                new Text(
                  'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Asperiores alias repudiandae reprehenderit sunt non consequatur repellendus! Dolores dolorum incidunt voluptate impedit in hic ea? Amet vel quos debitis veniam neque!',
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontWeight: FontWeight.normal,
                    fontSize: 15,
                  ),
                ),

                new Container(height: 5)
              ]
            )
          ),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            margin: EdgeInsets.only(bottom: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                // payment
                Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: _cardTitile('Pembayaran'),
                ),
                _cardInfo(Icons.check, 'Dibayar hari ini'),
                _cardInfo(Icons.check, 'Dibayar nanti'),
              ]
            )
          ),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            margin: EdgeInsets.only(bottom: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                // deliver
                Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: _cardTitile('Pengantaran'),
                ),
                // _cardInfo(Icons.check, 'Jasa angkut Qabayan'),
                _cardInfo(Icons.check, 'Diambil oleh Pembeli'),
                _cardInfo(Icons.check, 'Diantarkan oleh Penjual'),

              ]
            )
          ),


          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            margin: EdgeInsets.only(bottom: 15),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                new Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: _cardTitile('Dijual oleh'),
                ),
                new InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed('user-profile');
                  },
                  child: new Container(
                    child: new Row(
                      children: <Widget>[

                        new Container(
                          margin: EdgeInsets.only(right: 10),
                          width: 40,
                          height: 40,
                          color: OnColors.whiteGrey,
                          child: new ClipRRect(
                            borderRadius: new BorderRadius.circular(35),
                            child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                          ),
                        ),

                        new Container(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                'Green Tea',
                                style: TextStyle(
                                  color: OnColors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                              new Text(
                                'Pedagang',
                                style: TextStyle(
                                  color: OnColors.grey,
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal
                                ),
                              ),
                            ],
                          ),
                        ),

                        new Expanded(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              ButtonCircle(
                                action: () {
                                  Navigator.of(context).pushNamed('user-profile');
                                },
                                icon: Icons.chevron_right,
                                size: 32.0,
                                type: 'white',
                              )
                            ],
                          ),
                        )

                      ],
                    ),
                  ),
                ),

              ]
            )
          ),

          // new Container(
          //   color: Colors.white,
          //   padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
          //   margin: EdgeInsets.only(bottom: 15),
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: <Widget>[
          //       // pembelian
          //       Container(
          //         margin: EdgeInsets.only(bottom: 5),
          //         child: _cardTitile('Diskusi'),
          //       ),
          //       _cardNotes('Masih bingung tentang barang ini? Tanya langsung penjual aja, tapi pertanyaan kamu akan diperlihatkan secara publik.'),
          //       Container(
          //         margin: EdgeInsets.only(top: 10),
          //         width: double.infinity,
          //         child: ButtonText(
          //           action: () {
          //             Navigator.of(context).pushNamed('chats-history');
          //           },
          //           type: 'grey',
          //           title: 'Ngobrol dengan penjual?',
          //         ),
          //       ),
          //     ]
          //   )
          // ),

          // new Container(
          //   color: Colors.white,
          //   padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
          //   margin: EdgeInsets.only(bottom: 15),
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: <Widget>[
          //       // pembelian
          //       Container(
          //         margin: EdgeInsets.only(bottom: 5),
          //         child: _cardTitile('Nego'),
          //       ),
          //       _cardNotes('Sekarang kamu bisa memberikan penawaran ke penjual dengan harga terbaik.'),
          //       Container(
          //         margin: EdgeInsets.only(top: 10),
          //         width: double.infinity,
          //         child: ButtonText(
          //           action: () {
          //             Navigator.of(context).pushNamed('post-offers');
          //           },
          //           type: 'grey',
          //           title: 'Nego barang?',
          //         ),
          //       ),
          //     ]
          //   )
          // ),

          // new Container(
          //   color: Colors.white,
          //   padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
          //   margin: EdgeInsets.only(bottom: 15),
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: <Widget>[
          //       // pembelian
          //       Container(
          //         margin: EdgeInsets.only(bottom: 5),
          //         child: _cardTitile('Tawarkan barang'),
          //       ),
          //       _cardNotes('Tawarkan langsung kepada penjual langganan atau pilihan kamu.'),
          //       Container(
          //         margin: EdgeInsets.only(top: 10),
          //         width: double.infinity,
          //         child: ButtonText(
          //           action: () {},
          //           type: 'grey',
          //           title: 'Tawarkan barang?',
          //         ),
          //       ),
          //     ]
          //   )
          // ),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            margin: EdgeInsets.only(bottom: 15),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                // pembelian
                new Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: _cardTitile('Telpon atau SMS penjual?'),
                ),
                _cardNotes('* Pembelian barang bisa secara langsung dengan menghubungi kontak penjual yang sudah disediakan.'),
                new Container(
                  margin: EdgeInsets.only(top: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Expanded(
                        child: ButtonText(
                          action: () {},
                          type: 'white',
                          title: 'Telpon',
                        ),
                      ),
                      new Expanded(
                        child: ButtonText(
                          action: () {},
                          type: 'white',
                          title: 'SMS',
                        ),
                      ),
                      new Expanded(
                        child: ButtonText(
                          action: () {},
                          type: 'white',
                          title: 'Whatsapp',
                        )
                      )
                      // ButtonTextIcon(
                      //   btnColor: Colors.white,
                      //   btnIcon: Icons.phone,
                      //   btnRadius: 50.0,
                      //   btnText: 'Telpon',
                      //   btnTextColor: Colors.grey.shade800,
                      //   btnAction: () {},
                      // ),
                      // ButtonTextIcon(
                      //   btnColor: Colors.white,
                      //   btnIcon: Icons.mail,
                      //   btnRadius: 50.0,
                      //   btnText: 'SMS',
                      //   btnTextColor: Colors.grey.shade800,
                      //   btnAction: () {},
                      // ),
                      // ButtonTextIcon(
                      //   btnColor: Colors.white,
                      //   btnIcon: Icons.add_comment,
                      //   btnRadius: 50.0,
                      //   btnText: 'Whatsapp',
                      //   btnTextColor: Colors.grey.shade800,
                      //   btnAction: () {},
                      // ),
                    ],
                  ),
                ),
              ]
            )
          ),
          

        ],
      ),

      // bottomNavigationBar: BottomAppBar(
      //   color: Colors.white,
      //   elevation: 0.0,
      //   clipBehavior: Clip.none,
      //   child: new Container(
      //     child: new Row(
      //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //       children: <Widget>[
      //         new Expanded(
      //           child: ButtonText(
      //             btnText: 'Diskusi',
      //             btnColor: Colors.white,
      //             btnTextColor: Colors.grey.shade800,
      //             btnRadius: 0.0,
      //             btnAction: () {
      //               Navigator.of(context).pushNamed('post-comments');
      //             },
      //           ),
      //         ),
      //         new Expanded(
      //           child: ButtonText(
      //             btnText: 'Nego',
      //             btnColor: Colors.white,
      //             btnTextColor: Colors.grey.shade800,
      //             btnRadius: 0.0,
      //             btnAction: () {
      //               Navigator.of(context).pushNamed('post-offers');
      //             },
      //           ),
      //         ),
      //         new Expanded(
      //           child: ButtonText(
      //             btnText: 'Beli Sekarang',
      //             btnColor: OnColors.primary,
      //             btnTextColor: Colors.white,
      //             btnRadius: 0.0,
      //             btnAction: () {
      //               Navigator.of(context).pushNamed('make-order');
      //             },
      //           ),
      //         ),
      //       ],
      //     )
      //   ),
      // )

      bottomNavigationBar: new BottomAppBar(
        color: Colors.white,
        elevation: 0.0,
        clipBehavior: Clip.none,
        child: new Container(
          // padding: EdgeInsets.all(10.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              new InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed('chats-history');
                  },
                  child: new Container(
                    width: 100,
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.white
                    ),
                    child: new Container(
                      padding: EdgeInsets.only(top: 8, bottom: 8),
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Icon(
                            LineIcons.comments,
                            color: OnColors.black,
                            size: 24,
                          ),
                          new Text(
                            'Diskusi',
                            style: TextStyle(color: OnColors.black, fontSize: 12)
                          ),
                        ],
                      )
                    )
                  ),

              ),

              new Container(
                width: 0.5,
                height: 35,
                color: OnColors.grey
              ),

              new InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed('post-offers');
                  },
                  child: new Container(
                    width: 130,
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.white
                    ),
                    child: new Container(
                      padding: EdgeInsets.only(top: 8, bottom: 8),
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Icon(
                            LineIcons.cart_plus,
                            color: OnColors.black,
                            size: 24,
                          ),
                          new Text(
                            'Tambah Keranjang',
                            style: TextStyle(color: OnColors.black, fontSize: 12)
                          ),
                        ],
                      )
                    )
                  )

              ),

              // new Expanded(
              //   child: ButtonText(
              //     title: 'Beli Sekarang',
              //     type: 'main',
              //     action: () {
              //       Navigator.of(context).pushNamed('make-order');
              //     },
              //   ),
              // ),

              // new SizedBox(width: 15),

              new Expanded(
                child: new InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed('make-order');
                  },
                  child: new Container(
                    width: double.maxFinite,
                    height: 60,
                    decoration: BoxDecoration(
                      color: OnColors.main
                    ),
                    child: new Center(
                      child: new Text(
                        'Beli Sayuran',
                        style: TextStyle(color: Colors.white, fontSize: 16)
                      ),
                    )
                  )
                ),
              )

            ],
          )
        )
      )

    );
  }

  _cardNotes (var title) {
    return Text(
      title,
      style: TextStyle(
        color: Colors.grey.shade600,
        fontWeight: FontWeight.normal,
        fontSize: 14.0,
      ),
    );
  }

  _cardTitile (var title) {
    return Text(
      title, 
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 16,
        color: OnColors.black
      ),
      softWrap: true
    );
  }

  // _onStart (var value) {
  //   return Container(
  //     child: new Row(
  //       mainAxisAlignment: MainAxisAlignment.start,
  //       children: <Widget>[
  //         new Icon(
  //           Icons.star,
  //           color: Colors.yellow,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.yellow,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.yellow,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.grey.shade300,
  //           size: 16,
  //         ),
  //         new Icon(
  //           Icons.star,
  //           color: Colors.grey.shade300,
  //           size: 16,
  //         ),
  //         new SizedBox(width: 5),
  //         new Text(
  //           '4.6',
  //           style: TextStyle(
  //             fontSize: 14,
  //             fontWeight: FontWeight.normal,
  //             color: Colors.grey.shade500,
  //           ),
  //           softWrap: true
  //         ),
  //       ],
  //     )
  //   );
  // }

  _cardInfo (var icon, var title) {
    return new Container(
      margin: EdgeInsets.only(bottom: 3),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 5.0, right: 10.0),
            child: Icon(
              icon,
              size: 20,
              color: OnColors.main
            ),
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                fontSize: 14,
                color: OnColors.black
              ),
            ),
          )
        ],
      ),
    );
  }
  
}