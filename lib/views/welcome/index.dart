import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/services/banner.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  int _current = 0;
  List data;

  Future getData() async {
    var data = await loadBanner();
    setState(() {
      this.data = data;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(0), 
        child: new AppBar(
          backgroundColor: Colors.white,
        )
      ),
      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[

          // banner
          new Container(
            child: new CarouselSlider(
                  autoPlay: true,
                  height: 500,
                  enlargeCenterPage: false,
                  // autoPlayInterval: Duration(seconds: 60),
                  // autoPlayAnimationDuration: Duration(seconds: 60),
                  aspectRatio: 2.0,
                  items: [0,1,2,3].map((i) {
                    
                    // print(this.data);

                    return Builder(
                      builder: (BuildContext context) {
                        return new InkWell(
                          onTap: () {
                            Navigator.of(context).pushNamed('articles-view');
                          },
                          child: new Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.symmetric(horizontal: 15),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[

                                new SizedBox(height: 30),

                                new Container(
                                  width: double.maxFinite,
                                  height: 320,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(15)
                                  ),
                                  child: new ClipRect(
                                    child: Image.asset(this.data[i].image, fit: BoxFit.fitHeight),
                                  ),
                                ),

                                new SizedBox(height: 30),

                                new Text(
                                  this.data[i].title,
                                  style: new TextStyle(
                                    color: OnColors.main,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 26
                                  ),
                                ),

                                new SizedBox(height: 10),

                                new Container(
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                  child: new Text(
                                    this.data[i].subtitle,
                                    textAlign: TextAlign.center,
                                    style: new TextStyle(
                                      color: OnColors.black,
                                      fontWeight: FontWeight.normal,
                                      fontSize: 18
                                    ),
                                  ),
                                )

                              ],
                            ),
                          ),
                        );
                      }
                    );
                  }).toList(),
                  onPageChanged: (index) {
                    setState(() {
                      _current = index;
                    });
                  },
            ),
          ),

          // join
          new Container(
            padding: EdgeInsets.only(left: 25, right: 25, top: 20, bottom: 20),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Text(
                  'Bergabung sekarang?',
                  style: new TextStyle(
                    color: OnColors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20
                  ),
                ),

                new SizedBox(height: 20),

                ButtonText(
                  title: 'Daftar & buat akun',
                  type: 'main',
                  action: () {
                    Navigator.of(context).pushNamed('register');
                  },
                ),

                new SizedBox(height: 20),

                ButtonText(
                  title: 'Login',
                  type: 'grey',
                  action: () {
                    Navigator.of(context).pushNamed('login');
                  },
                ),

                new SizedBox(height: 20),
                
              ],
            )
          ),

        ],
      ),
    );
  }

}