import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class Session {
  // setter
  static setUserToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', value);
  }
  static setUserID(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('id', value);
  }
  static setUserUsername(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('username', value);
  }

  // getter
  static Future<dynamic> getUserToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token') ?? 'token';
  }
  static Future<dynamic> getUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('id') ?? 'id';
  }
  static Future<dynamic> getUserUsername() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('username') ?? 'username';
  }

  // remove
  static Future<bool> removeUserToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove('token');
  }
  static Future<bool> removeUserUsername() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove('username');
  }
  static Future<bool> removeUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove('id');
  }

  // check
  static Future<bool> checkUserToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey('token') ?? false;
  }
  static Future<bool> checkUserUsername() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey('username') ?? false;
  }
  static Future<bool> checkUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey('id') ?? false;
  }
  
}