import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class ButtonTextIcon extends StatelessWidget {
  final title;
  final icon;
  final type;
  final action;

  ButtonTextIcon({
    Key key,
    @required this.title,
    @required this.icon,
    @required this.type,
    @required this.action
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var buttonColor;
    var textColor;

    if (this.type == 'main') {
      buttonColor = OnColors.main;
      textColor = Colors.white;
    }

    if (this.type == 'main-reverse') {
      buttonColor = Colors.white;
      textColor = OnColors.main;
    }

    if (this.type == 'primary') {
      buttonColor = OnColors.primary;
      textColor = OnColors.black;
    }

    if (this.type == 'secondary') {
      buttonColor = OnColors.secondary;
      textColor = Colors.white;
    }

    if (this.type == 'tertiary') {
      buttonColor = OnColors.tertiary;
      textColor = Colors.white;
    }

    if (this.type == 'grey') {
      buttonColor = OnColors.whiteGrey;
      textColor = OnColors.black;
    }

    if (this.type == 'white') {
      buttonColor = Colors.white;
      textColor = OnColors.black;
    }

    return new ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      child: new MaterialButton(
        onPressed: this.action,
        minWidth: double.maxFinite,
        height: 45,
        color: buttonColor,
        elevation: 0,
        child: new Row(
          children: <Widget>[
            new Icon(
              this.icon,
              color: textColor,
              size: 24,
            ),
            new SizedBox(width: 10),
            new Text(
              this.title,
              style: new TextStyle(
                color: textColor,
                fontSize: 16
              ),
            )
          ],
        )
      ),
    );
  }
}