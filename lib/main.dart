import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/onColors.dart';
// import 'package:qabayan_ui/views/welcome/index.dart';
import 'package:qabayan_ui/views/layouts/index.dart';


// routes
// transactions
import 'package:qabayan_ui/views/transactions/orders/detail.dart';
import 'package:qabayan_ui/views/transactions/sales/detail.dart';

// compose
import 'package:qabayan_ui/views/compose/index.dart';
import 'package:qabayan_ui/views/compose/paymentMethod.dart';
import 'package:qabayan_ui/views/compose/deliveryMethod.dart';
import 'package:qabayan_ui/views/compose/categoryCompose.dart';
import 'package:qabayan_ui/views/compose/typeCompose.dart';
import 'package:qabayan_ui/views/compose/unitCompose.dart';

// posts
import 'package:qabayan_ui/views/posts/index.dart';
import 'package:qabayan_ui/views/cart/index.dart';

// chats
import 'package:qabayan_ui/views/chats/index.dart';
import 'package:qabayan_ui/views/chats/history/index.dart';

// bergain
import 'package:qabayan_ui/views/bergains/index.dart';
import 'package:qabayan_ui/views/bergains/orders/index.dart';
import 'package:qabayan_ui/views/bergains/sales/index.dart';

// accounts
import 'package:qabayan_ui/views/account/profile.dart';

// order
import 'package:qabayan_ui/views/orders/index.dart';

// search
import 'package:qabayan_ui/views/search/index.dart';

// category
import 'package:qabayan_ui/views/categories/index.dart';

// auth
import 'package:qabayan_ui/views/auth/login.dart';
import 'package:qabayan_ui/views/auth/register.dart';
import 'package:qabayan_ui/views/auth/forgotPassword.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Qabayan',
      theme: new ThemeData(
        primarySwatch: OnColors.teal,
      ),
      home: new Layout(), //new WelcomePage(),
      routes: <String, WidgetBuilder> {

        // auth
        'login': (BuildContext context) => new LoginPage(),
        'register': (BuildContext context) => new RegisterPage(),
        'forgot-password': (BuildContext context) => new ForgotPasswordPage(),

        'cart': (BuildContext context) => new CartPage(),

        // home
        'home': (BuildContext context) => new Layout(),

        // compose
        'compose': (BuildContext context) => new ComposePage(),
        'compose-payment': (BuildContext context) => new PaymentComposeMethodPage(),
        'compose-delivery': (BuildContext context) => new DeliveryComposeMethodPage(),
        'compose-category': (BuildContext context) => new CategoryComposePage(),
        'compose-type': (BuildContext context) => new TypeComposePage(),
        'compose-unit': (BuildContext context) => new UnitComposePage(),

        // chats
        'chats': (BuildContext context) => new ChatsPage(),
        'chats-history': (BuildContext context) => new ChatsHistoryPage(),

        // bergains
        'bergains': (BuildContext context) => new BergainsPage(),
        'bergains-order': (BuildContext context) => new BergainOrderPage(),
        'bergains-sales': (BuildContext context) => new BergainSalePage(),

        // transactions
        'detail-orders': (BuildContext context) => new DetailOrderPage(),
        'detail-sales': (BuildContext context) => new DetailSalesPage(),

        // posts
        'detail-post': (BuildContext context) => new DetailPostPage(),

        // accounts
        'user-profile': (BuildContext context) => new ProfilePage(),

        // orders
        'make-order': (BuildContext context) => new MakeOrderPage(),

        // search
        'search': (BuildContext context) => new SearchPage(),

        // category
        'category': (BuildContext context) => new CategoryPage(),
        

      },
    );
  }
}

