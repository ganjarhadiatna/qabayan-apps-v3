import 'package:flutter/material.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onColors.dart';
// import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/views/cart/card.dart';

class ListKiloanPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // return new Container(
    //   child: new ListView(
    //     physics: ScrollPhysics(),
    //     children: <Widget>[
    //       new SizedBox(height: 15),
    //       CardCart(type: 'kiloan', value: 5),
    //       new SizedBox(height: 15),
    //       CardCart(type: 'kiloan', value: 5),
    //       new SizedBox(height: 15),
    //       CardCart(type: 'kiloan', value: 5),
    //       new SizedBox(height: 15),
    //     ],
    //   ),
    // );
    return new Scaffold(
      backgroundColor: OnColors.whiteGrey,
      body: new Container(
        child: new ListView(
          physics: ScrollPhysics(),
          children: <Widget>[
            new SizedBox(height: 15),
            CardCart(type: 'kiloan', value: 5),
            new SizedBox(height: 15),
            CardCart(type: 'kiloan', value: 5),
            new SizedBox(height: 15),
            CardCart(type: 'kiloan', value: 5),
            new SizedBox(height: 15),
          ],
        ),
      ),
      bottomNavigationBar: new BottomAppBar(
        color: Colors.white,
        elevation: 0.0,
        clipBehavior: Clip.none,
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              new Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Jumlah pembayaran',
                      style: TextStyle(
                        color: OnColors.black,
                        fontWeight: FontWeight.normal,
                        fontSize: 14
                      ),
                    ),
                    new SizedBox(height: 2.5),
                    new Text(
                      'IDR 0',
                      style: TextStyle(
                        color: OnColors.tertiary,
                        fontWeight: FontWeight.bold,
                        fontSize: 24
                      ),
                    ),
                  ],
                ),
              ),

              new Container(
                width: 150,
                child: ButtonText(
                  action: () {
                    Navigator.of(context).pushNamed('make-order');
                  },
                  type: 'main',
                  title: 'Buat Pesanan',
                ),
              )


            ],
          )
        )
      ),
    );
  }
}