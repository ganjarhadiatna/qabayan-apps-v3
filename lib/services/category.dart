import 'dart:async' show Future;
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:qabayan_ui/models/category.dart';

Future<String> _loadCategory() async {
  return await rootBundle.loadString('assets/jsons/category.json');
}

Future loadCategory() async {
  String jsonString = await _loadCategory();
  final jsonResponse = json.decode(jsonString);
  CategoryList category = new CategoryList.fromJson(jsonResponse);
  return category.categories;
}