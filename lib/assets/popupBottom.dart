import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';

class PopupBottom extends StatelessWidget {

  final String title;
  final Widget content;
  final bool buttonClose;

  PopupBottom({
    Key key,
    @required this.title,
    @required this.content,
    this.buttonClose = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
      alignment: Alignment(0,1),
      child: new Container(
        width: double.infinity,
        padding: EdgeInsets.only(top: 15, left: 15, right: 15, bottom: 15),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          )
        ),
        child: new Material(
          color: Colors.transparent,
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              new Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Text(
                      this.title,
                      style: TextStyle(
                        color: Colors.grey.shade800,
                        fontSize: 20,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    _onButtonClose(context)
                  ],
                ),
              ),

              new SizedBox(height: 10),

              new Container(
                child: this.content,
              )

            ],
          ),
        ),
      ),
    );
  }

  _onButtonClose(BuildContext context) {
    Widget btn = new ButtonCircle(
      icon: LineIcons.close, 
      size: 24.0, 
      type: 'white', 
      action: () {
       Navigator.of(context).pop();
      }
    );
    // this.buttonClose 
    // ? btn = new ButtonCircle(
    //     icon: LineIcons.close, 
    //     size: 24.0, 
    //     type: 'white', 
    //     action: () {
    //      Navigator.of(context).pop();
    //     }
    //   )
    // : btn = new Container();
    return btn;
  }
}