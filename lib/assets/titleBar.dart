import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class TitleBar extends StatelessWidget {
  final title;
  
  TitleBar({
    Key key,
    @required this.title, 
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Text(
        this.title, 
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
          color: OnColors.black
        ),
        softWrap: true
      ),
    );
  }
}