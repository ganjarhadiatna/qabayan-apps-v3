import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/views/inbox/card.dart';

class InboxPage extends StatefulWidget {
  @override
  _InboxPageState createState() => _InboxPageState();
}

class _InboxPageState extends State<InboxPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            new SizedBox(width: 20),

            new Expanded(
              child: new Container (
                padding: EdgeInsets.only(top: 17),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      'Notifikasi',
                      style: new TextStyle(
                        color: OnColors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 28
                      ),
                    )
                  ],
                ),
              ),
            ),

            new Container(
              padding: EdgeInsets.only(top: 12.5),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ButtonCircle(
                    action: () {},
                    icon: LineIcons.search,
                    size: 26.0,
                    type: 'white',
                  ),
                  ButtonCircle(
                    action: () {
                      Navigator.of(context).pushNamed('chats');
                    },
                    icon: LineIcons.comments_o,
                    size: 26.0,
                    type: 'white',
                  ),
                  ButtonCircle(
                    action: () {
                      Navigator.of(context).pushNamed('cart');
                    },
                    icon: LineIcons.shopping_cart,
                    size: 32.0,
                    type: 'white',
                  )
                ]
              )
            ),

            new SizedBox(width: 10)
          ],
        ),
      ),
      body: new Container(
        child: _onPage([
          _onTitle('Hari ini'),
          CardActivitesWidget(
            cardTitle: 'Memberikan sebuah penawaran pada barang yang kamu sedang jual.',
            cardType: 'offers',
          ),
          _onTitle('Kemarin'),
          CardActivitesWidget(
            cardTitle: 'Menambahkan barang yang kamu jual ke keranjang.',
            cardType: 'detail',
          ),
          CardActivitesWidget(
            cardTitle: 'Mengirimkan pesan pada barang kamu jual.',
            cardType: 'comments',
          ),
          CardActivitesWidget(
            cardTitle: 'Hi.. Cabai rawit baru panen hanya untuk kamu.',
            cardType: 'detail',
          ),
          _onTitle('22 Agustus 2019'),
          CardActivitesWidget(
            cardTitle: 'Mengirimkan pesan pada barang kamu jual.',
            cardType: 'comments',
          ),
          CardActivitesWidget(
            cardTitle: 'Mengirimkan pesan pada barang kamu jual.',
            cardType: 'comments',
          ),
        ]),
      ),
      // body: new DefaultTabController(
      //   length: 3,
      //   initialIndex: 0,
      //   child: Scaffold(
      //     backgroundColor: Colors.white,
      //     appBar: new PreferredSize(
      //       preferredSize: Size(double.infinity, 60),
      //       child: new Container(
      //         margin: EdgeInsets.only(top: 10, bottom: 10),
      //         child: new TabBar(
      //           unselectedLabelColor: Colors.grey.shade600,
      //           labelColor: Colors.white,
      //           indicatorSize: TabBarIndicatorSize.label,
      //           indicator: BoxDecoration(
      //             borderRadius: BorderRadius.circular(50),
      //             color: OnColors.main,
      //             border: Border.all(
      //               width: 1,
      //               color: OnColors.main
      //             )
      //           ),
      //           tabs: <Widget>[
      //             new Tab(
      //               child: new Container(
      //                 child: Align(
      //                   alignment: Alignment.center,
      //                   child: Text('Aktifitas'),
      //                 ),
      //               ),
      //             ),
      //             new Tab(
      //               child: new Container(
      //                 child: Align(
      //                   alignment: Alignment.center,
      //                   child: Text('Ngobrol'),
      //                 ),
      //               ),
      //             ),
      //             new Tab(
      //               child: new Container(
      //                 child: Align(
      //                   alignment: Alignment.center,
      //                   child: Text('Nego'),
      //                 ),
      //               ),
      //             ),
      //           ]
      //         ),
      //       )
      //     ),
      //     body: TabBarView(
      //       children: <Widget>[

      //         _onPage([
      //           _onTitle('22 Agustus 2019'),
      //           CardActivitesWidget(
      //             cardTitle: 'Menyukai barang yang kamu jual.',
      //             cardType: 'detail',
      //           ),
      //           CardActivitesWidget(
      //             cardTitle: 'Hi.. Cabai rawit baru panen hanya untuk kamu.',
      //             cardType: 'detail',
      //           ),
      //         ]),

      //         // EmptyPage(
      //         //   title: 'Inbox pembelian masih kosong, silahkan lakukan pembelian barang.',
      //         //   icon: Icons.indeterminate_check_box,
      //         // ),

      //         _onPage([
      //           _onTitle('22 Agustus 2019'),
      //           CardActivitesWidget(
      //             cardTitle: 'Mengirimkan pesan pada barang kamu jual.',
      //             cardType: 'comments',
      //           ),
      //           CardActivitesWidget(
      //             cardTitle: 'Mengirimkan pesan pada barang kamu jual.',
      //             cardType: 'comments',
      //           ),
      //           CardActivitesWidget(
      //             cardTitle: 'Mengirimkan pesan pada barang kamu jual.',
      //             cardType: 'comments',
      //           ),
      //         ]),

      //         _onPage([
      //           _onTitle('22 Agustus 2019'),
      //           CardActivitesWidget(
      //             cardTitle: 'Memberikan sebuah penawaran pada barang yang kamu sedang jual.',
      //             cardType: 'offers',
      //           )
      //         ]),
      //       ],
      //     ),
      //   ),
      // ),
    );
  }

  _onPage (List<Widget> contents) {
    return new Container(
      color: Colors.grey.shade100,
      child: ListView.builder(
        padding: EdgeInsets.only(top: 10, bottom: 5),
        itemCount: 1,
        itemBuilder: (context, position) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: contents
          );
        }
      )
    );
  }

  _onTitle (var title) {
    return Container (
      margin: EdgeInsets.fromLTRB(20, 5, 20, 10),
      child: Text(
        title,
        style: TextStyle(
          color: OnColors.black,
          fontSize: 16,
          fontWeight: FontWeight.normal,
        )
      ),
    );
  }
}