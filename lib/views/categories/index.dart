import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/views/explore/index.dart';
import 'package:qabayan_ui/views/search/cardSearch.dart';
// import 'package:qabayan_ui/views/search/result.dart';

class CategoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Kategori Barang'),
                    
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          ButtonCircle(
                            size: 24.0,
                            type: 'white',
                            icon: LineIcons.search,
                            action: () {
                              Navigator.of(context).pushNamed('search');
                            }
                          ),
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: LineIcons.shopping_cart,
                            action: () {
                              Navigator.of(context).pushNamed('cart');
                            }
                          ),
                        ]
                      )
                    )

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new Container(
        color: Colors.grey.shade100,
        child: new ListView(
          children: <Widget>[
            _onTitle('Kategori populer'),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            

            _onTitle('Semua kategori'),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            OnLine(),
            CardSearch(
              title: 'Kategori barang', 
              icon: Icons.star,
              action: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ExplorePage(
                      index: 0,
                      title: 'Discover'
                    )
                  )
                );
              }),
            
            new SizedBox(height: 15),
          ],
        ),
      ),

    );
  }

  _onTitle (var title) {
    return Container (
      margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
      child: Text(
        title,
        style: TextStyle(
          color: OnColors.black,
          fontSize: 16,
          fontWeight: FontWeight.normal,
        )
      ),
    );
  }

}