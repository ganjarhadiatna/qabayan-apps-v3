import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onLine.dart';
// import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
// import 'package:qabayan_ui/assets/buttonTextIcon.dart';
import 'package:qabayan_ui/assets/onColors.dart';
// import 'package:qabayan_ui/assets/popupBottom.dart';

class DetailOrderPage extends StatefulWidget {
  @override
  _DetailOrderPageState createState() => _DetailOrderPageState();
}

class _DetailOrderPageState extends State<DetailOrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[

            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Detail Pembelian'),
                    
                    // Expanded(
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.end,
                    //     children: <Widget>[
                    //       ButtonCircle(
                    //         size: 28.0,
                    //         type: 'white',
                    //         icon: Icons.more_vert,
                    //         action: () {
                              // showDialog(
                              //   context: context,
                              //   builder: (BuildContext context) => PopupBottom(
                              //     title: 'Opsi',
                              //     content: new Column(
                              //       children: <Widget>[
                              //         ButtonTextIcon(
                              //           btnAction: () {
                              //             Navigator.of(context).pushNamed('post-offers');
                              //           },
                              //           btnColor: Colors.white,
                              //           btnIcon: Icons.blur_circular,
                              //           btnRadius: 0.0,
                              //           btnText: 'Batalkan Pemesanan',
                              //           btnTextColor: Colors.grey.shade800,
                              //         ),
                              //         ButtonTextIcon(
                              //           btnAction: () {},
                              //           btnColor: Colors.white,
                              //           btnIcon: Icons.blur_circular,
                              //           btnRadius: 0.0,
                              //           btnText: 'Hapus Pemesanan',
                              //           btnTextColor: Colors.grey.shade800,
                              //         ),
                              //       ]
                              //     ),
                              //   ),
                              // );
                    //         },
                    //       ),
                    //     ],
                    //   )
                    // ),
                  ],
                )
              )
            ),

          ],
        ),
      ),

      body: new ListView(
        children: <Widget>[

          new SizedBox(height: 15),

            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed('detail-post');
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 35,
                      height: 35,
                      color: OnColors.whiteGrey,
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(5),
                        // child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  new Container(
                    child: new Text(
                      'Green Tea',
                      style: TextStyle(
                        color: OnColors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal
                      )
                    ),
                  ),
                  new Expanded(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        ButtonCircle(
                          action: () {
                            Navigator.of(context).pushNamed('chats-history');
                          },
                          icon: LineIcons.comments,
                          size: 22.0,
                          type: 'white',
                        ),
                        ButtonCircle(
                          action: () {
                            Navigator.of(context).pushNamed('detail-post');
                          },
                          icon: Icons.chevron_right,
                          size: 28.0,
                          type: 'white',
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            
            OnLine(),

            new Container(
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              color: Colors.white,
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[

                  new InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed('detail-post');
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 70,
                      height: 70,
                      color: OnColors.whiteGrey,
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(5),
                        // child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                      ),
                    ),
                  ),

                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          'Cabai rawit merah',
                          style: TextStyle(
                            color: Colors.grey.shade800,
                            fontSize: 18,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        new SizedBox(height: 5),
                        _onInfo ('Harga', 'Rp 15.000', OnColors.tertiary),
                        _onInfo ('Stok', 'Sisa 30 Kg', OnColors.black),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          
          new SizedBox(height: 15),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: _cardMainTitile('Detail Transaksi'),
          ),
          OnLine(),
          new Container(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 15),
            color: Colors.white,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _onInfo ('Pembayaran', 'Dibayar hari ini', OnColors.black),
                _onInfo ('Pengiriman', 'Diambil oleh pembeli', OnColors.black),
                new Container(
                  padding: EdgeInsets.only(top: 15, bottom: 15),
                  child: OnLine(),
                ),
                _onInfo ('Harga', 'Rp 13.000', OnColors.black),
                _onInfo ('Jumlah', '2 Kg', OnColors.black),
                _onInfo ('Total biaya', 'Rp 26.000', OnColors.tertiary),
              ],
            ),
          ),

          new SizedBox(height: 15),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: _cardMainTitile('Tanggal'),
          ),
          OnLine(),
          new Container(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 15),
            color: Colors.white,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _onInfo ('Tanggal pesanan', 'Selasa, 6 Agustus 2019', OnColors.black),
                _onInfo ('Sampai pada', 'Kamis, 7 Agustus 2019', OnColors.black),
              ],
            )
          ),

          new SizedBox(height: 15),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: _cardMainTitile('Status pesanan'),
          ),
          OnLine(),
          new Container(
            padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
            color: Colors.white,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _onInfo ('Sampai mana?', 'Dalam perjalanan', OnColors.tertiary),
              ],
            ),
          ),

          new SizedBox(height: 15),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: _cardMainTitile('Pesanan berhasil?'),
          ),
          OnLine(),
          new Container(
            padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
            color: Colors.white,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _cardNotes('Tekan tombol ini jika pesanan kamu sudah sampai ditempat dengan selamat.'),
                new Container(
                  margin: EdgeInsets.only(top: 10),
                  width: double.infinity,
                  child: ButtonText(
                    action: () {},
                    type: 'main',
                    title: 'Pesanan Sudah Sampai',
                  ),
                ),
              ],
            ),
          ),

          SizedBox(height: 15),

          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: _cardMainTitile('Batalkan pesanan?'),
          ),
          OnLine(),
          new Container(
            padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
            color: Colors.white,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _cardNotes('Pembatalan pesanan hanya bisa dilakukan oleh penjual saja, tombol ini hanya sebuah permintaan pembatalan pesanan yang akan dikirim ke penjual.'),
                new Container(
                  margin: EdgeInsets.only(top: 10),
                  width: double.infinity,
                  child: ButtonText(
                    action: () {},
                    type: 'grey',
                    title: 'Batalkan Pesanan',
                  ),
                ),
              ],
            ),
          ),

          SizedBox(height: 15),

        ],
      ),
      
    );
  }

  

  _cardNotes (var title) {
    return Text(
      title,
      style: TextStyle(
        color: Colors.grey.shade600,
        fontWeight: FontWeight.normal,
        fontSize: 14.0,
      ),
    );
  }

  _cardMainTitile (var title) {
    return Text(
      title, 
      style: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 16.0,
        color: Colors.grey.shade800
      ),
      softWrap: true
    );
  }

  // _cardTitile (var title) {
  //   return Text(
  //     title, 
  //     style: TextStyle(
  //       fontWeight: FontWeight.bold,
  //       fontSize: 16.0,
  //       color: Colors.grey.shade800
  //     ),
  //     softWrap: true
  //   );
  // }

  // _onLine () {
  //   return Container(
  //     margin: EdgeInsets.only(top: 10),
  //     height: 1,
  //     color: Colors.grey.shade300,
  //   );
  // }

  // _onTitle (var title) {
  //   return Container (
  //     margin: EdgeInsets.fromLTRB(10, 5, 10, 10),
  //     child: Text(
  //       title,
  //       style: TextStyle(
  //         color: Colors.grey.shade800,
  //         fontSize: 16,
  //         fontWeight: FontWeight.normal,
  //       )
  //     ),
  //   );
  // }

  _onInfo (var title, var value, var valColor) {
    return Container(
      margin: EdgeInsets.only(bottom: 2.5, top: 2.5),
      child: Row(
        children: <Widget>[

          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontWeight: FontWeight.normal,
                    fontSize: 16
                  ),
                )
              ],
            ),
          ),

          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  value,
                  style: TextStyle(
                    color: valColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16
                  ),
                )
              ],
            ),
          ),

        ],
      ),
    );
  }


}