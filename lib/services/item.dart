import 'dart:async' show Future;
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:qabayan_ui/models/item.dart';

Future<String> _loadItem() async {
  return await rootBundle.loadString('assets/jsons/item.json');
}

Future loadItem() async {
  String jsonString = await _loadItem();
  final jsonResponse = json.decode(jsonString);
  ItemList item = new ItemList.fromJson(jsonResponse);
  return item.items;
}