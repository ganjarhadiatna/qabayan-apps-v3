import 'dart:async';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/views/posts/cardGrider.dart';
import 'package:qabayan_ui/services/item.dart';
import 'package:qabayan_ui/views/posts/cardList.dart';

class ExplorePage extends StatefulWidget {
  final index;
  final title;

  ExplorePage({
    Key key,
    @required this.index,
    @required this.title,
  }) : super(key: key);

  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  List dataItem;

  Future getDataItem() async {
    var dataItem = await loadItem();
    setState(() {
      this.dataItem = dataItem;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getDataItem();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: widget.title),
                    
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          ButtonCircle(
                            size: 24.0,
                            type: 'white',
                            icon: LineIcons.search,
                            action: () {
                              Navigator.of(context).pushNamed('search');
                            }
                          ),
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: LineIcons.shopping_cart,
                            action: () {
                              Navigator.of(context).pushNamed('cart');
                            }
                          ),

                        ],
                      ),
                    ),

                    new SizedBox(width: 10),

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: new PreferredSize(
            preferredSize: Size(double.infinity, 60),
            child: new Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: new TabBar(
                unselectedLabelColor: Colors.grey.shade600,
                labelColor: Colors.white,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: OnColors.main,
                  border: Border.all(
                    width: 1,
                    color: OnColors.main
                  )
                ),
                tabs: <Widget>[
                  new Tab(
                    child: new Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Kiloan'),
                      ),
                    ),
                  ),
                  new Tab(
                    child: new Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Borongan'),
                      ),
                    ),
                  ),
                ]
              ),
            )
          ),
          body: new Container(
            color: Colors.grey.shade100,
            child: new TabBarView(
              children: <Widget>[
                _onListItem(5),
                _onListItem(5)
              ],
            ),
          ),
        ),
      )

    );
  }

  _onListItem (var val) {
    return new Container(
      // child: new GridView.builder(
      //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      //     crossAxisCount: 2,
      //     childAspectRatio: MediaQuery.of(context).size.width / (MediaQuery.of(context).size.height / 1.35)
      //   ), 
      //   itemCount: val,
      //   padding: EdgeInsets.all(5),
      //   itemBuilder: (context, index) {
      //     return CardGriderWidget(
      //       image: this.dataItem[index].image,
      //       title: this.dataItem[index].title,
      //       price: this.dataItem[index].price,
      //       status: this.dataItem[index].status
      //     );
      //   }
      // )
      child: new ListView.builder(
        padding: EdgeInsets.only(top: 15),
        itemCount: val,
        itemBuilder: (context, index) {
          return CardListWidget(
            image: this.dataItem[index].image,
            title: this.dataItem[index].title,
            price: this.dataItem[index].price,
            secondPrice: this.dataItem[index].secondPrice,
            status: this.dataItem[index].status,
            category: this.dataItem[index].category,
            location: this.dataItem[index].location
          );
        },
      ),
    );
  }
}