import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:qabayan_ui/views/auth/login.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/views/bergains/card.dart';

class BergainsPage extends StatefulWidget {
  @override
  _BergainsPageState createState() => _BergainsPageState();
}

class _BergainsPageState extends State<BergainsPage> {
  String _id;

  Future getUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString('id');
    setState(() {
      this._id = id;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getUserID();
  }

  @override
  Widget build(BuildContext context) {
    return (this._id == null) ? LoginPage() : _onLayout();
  }

  _onLayout() {
    return new Scaffold(
      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Negosiasi'),

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: new PreferredSize(
            preferredSize: Size(double.infinity, 60),
            child: new Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: new TabBar(
                unselectedLabelColor: Colors.grey.shade600,
                labelColor: Colors.white,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: OnColors.main,
                  border: Border.all(
                    width: 1,
                    color: OnColors.main
                  )
                ),
                tabs: <Widget>[
                  new Tab(
                    child: new Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Pembelian'),
                      ),
                    ),
                  ),
                  new Tab(
                    child: new Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Penjualan'),
                      ),
                    ),
                  ),
                ]
              ),
            )
          ),
          body: new Container(
            color: OnColors.whiteGrey,
            child: new TabBarView(
              children: <Widget>[

                CardBergainsWidget(val: 'bergains-order'),
                CardBergainsWidget(val: 'bergains-sales')

              ],
            ),
          ),
        ),
      ),

    );
  }

}