import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class ButtonText extends StatelessWidget {
  final title;
  final type;
  final action;

  ButtonText({
    Key key,
    @required this.title,
    @required this.type,
    @required this.action
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var buttonColor;
    var textColor;

    if (this.type == 'main') {
      buttonColor = OnColors.main;
      textColor = Colors.white;
    }

    if (this.type == 'main-reverse') {
      buttonColor = Colors.white;
      textColor = OnColors.main;
    }

    if (this.type == 'primary') {
      buttonColor = OnColors.primary;
      textColor = OnColors.black;
    }

    if (this.type == 'secondary') {
      buttonColor = OnColors.secondary;
      textColor = Colors.white;
    }

    if (this.type == 'tertiary') {
      buttonColor = OnColors.tertiary;
      textColor = Colors.white;
    }

    if (this.type == 'grey') {
      buttonColor = OnColors.whiteGrey;
      textColor = OnColors.black;
    }

    if (this.type == 'white') {
      buttonColor = Colors.white;
      textColor = OnColors.black;
    }

    if (this.type == 'red') {
      buttonColor = Colors.white;
      textColor = OnColors.red;
    }

    return new MaterialButton(
        onPressed: this.action,
        minWidth: double.maxFinite,
        height: 45,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5)
        ),
        color: buttonColor,
        elevation: 0,
        child: new Text(
          this.title,
          style: new TextStyle(
            color: textColor,
            fontSize: 16
          ),
        )
    );
  }
}