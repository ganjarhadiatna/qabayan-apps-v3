import 'dart:async';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/titleContent.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/views/explore/index.dart';
import 'package:qabayan_ui/views/posts/cardSlider.dart';
import 'package:qabayan_ui/services/item.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  List dataItem;

  Future getDataItem() async {
    var dataItem = await loadItem();
    setState(() {
      this.dataItem = dataItem;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getDataItem();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),
                    TitleBar(title: 'Profil Pengguna'),
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          ButtonCircle(
                            size: 26.0,
                            type: 'white',
                            icon: LineIcons.search,
                            action: () {
                              Navigator.of(context).pushNamed('search');
                            }
                          ),
                          ButtonCircle(
                            action: () {
                              Navigator.of(context).pushNamed('chats-history');
                            },
                            icon: LineIcons.comments_o,
                            size: 26.0,
                            type: 'white',
                          ),
                          ButtonCircle(
                            size: 32.0,
                            type: 'white',
                            icon: LineIcons.shopping_cart,
                            action: () {}
                          ),
                        ],
                      ),
                    ),
                    new SizedBox(width: 10),
                  ],
                )
              )
            )

          ],
        ),
      ),
      body: new ListView(
          children: <Widget>[

            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 5.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  new Container(
                    width: 100,
                    height: 100,
                    color: OnColors.whiteGrey,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(100),
                      child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                    ),
                  ),

                ],
              ),
            ),

            new Container(
              color: Colors.white,
              padding: EdgeInsets.only(top: 10, bottom: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Text(
                    'Ganjar Hadiatna',
                    style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 32,
                      fontWeight: FontWeight.bold
                    ),
                  ),

                  SizedBox(height: 10),

                  Text(
                    'Pengembang bisnis dalam bidang agrikultur',
                    style: TextStyle(
                      color: Colors.grey.shade600,
                      fontSize: 16,
                      fontWeight: FontWeight.normal
                    ),
                  ),

                ],
              ),
            ),

            new Container(
              color: Colors.white,
              height: 60,
              margin: EdgeInsets.only(bottom: 25),
              child: new Column(
                children: <Widget>[
                  new GridView.count(
                    shrinkWrap: true,
                    primary: true,
                    physics: new NeverScrollableScrollPhysics(),
                    crossAxisCount: 3,
                    childAspectRatio: 2.4,
                    children: <Widget>[
                      _onCategory(Icons.work, OnColors.magenta, 'Mahasiswa', () {}),
                      _onCategory(Icons.phone, OnColors.teal, '+62896-9389-9869', () {}),
                      _onCategory(Icons.motorcycle, OnColors.tertiary, 'Kp. Areng', () {}),
                    ],
                  ),
                ],
              )
            ),

            new Container(
              margin: EdgeInsets.only(left: 15, right: 15, top: 0),
              child: TitleContent(
                title: 'Sedang dijual',
                subtitle: '',
                more: 'Lainnya',
                action: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ExplorePage(
                        index: 0,
                        title: 'Sedang Dijual'
                      )
                    )
                  );
                },
              ),
            ),
            new Container(
              margin: EdgeInsets.only(top: 15, bottom: 0),
              child: _onSlideItem(5),
            ),

            new Container(
              margin: EdgeInsets.only(left: 15, right: 15, top: 20),
              child: TitleContent(
                title: 'Barang terlaris',
                subtitle: '',
                more: 'Lainnya',
                action: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ExplorePage(
                        index: 0,
                        title: 'Barang Terlaris'
                      )
                    )
                  );
                },
              ),
            ),
            new Container(
              margin: EdgeInsets.only(top: 15, bottom: 20),
              child: _onSlideItem(5),
            ),
          ],
      ),
    );
  }

  _onSlideItem (var val) {
    return new Container(
      height: 240,
      child: new ListView(
        padding: EdgeInsets.only(left: 15),
        scrollDirection: Axis.horizontal,
        children: List.generate(val, (index) {
          return CardSlider(
            title: this.dataItem[index].title,
            image: this.dataItem[index].image,
            price: this.dataItem[index].price,
            status: this.dataItem[index].status
          );
        })
      ),
    );
  }

  _onCategory (var icon, var color, var title, var action) {
    return new Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0)
      ),
      elevation: 0.0,
      // margin: EdgeInsets.all(10.0),
      child: InkWell(
        onTap: action,
        child: Container(
            color: Colors.white,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 10.0),
                    child: Icon(
                      icon,
                      color: color,
                      size: 22,
                    ),
                  ),
                  Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 16,
                    ),
                  )
                ],
              ),
            ),
        ),
      ),
    );
  }
}