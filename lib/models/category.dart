class CategoryList {
  final List<Category> categories;
  CategoryList({
    this.categories
  });
  factory CategoryList.fromJson(List<dynamic> parsedJson) {
    List<Category> categories = new List<Category>();
    categories = parsedJson.map((i) => Category.fromJson(i)).toList();
    return new CategoryList(
      categories: categories
    );
  }
}

class Category {
  final String id;
  final String image;
  final String title;
  final String totalItem;
  Category({
    this.id,
    this.image,
    this.title,
    this.totalItem
  });
  factory Category.fromJson(Map<String, dynamic> parsedJson) {
    return Category(
      id: parsedJson["id"].toString(),
      image: parsedJson["image"],
      title: parsedJson["title"],
      totalItem: parsedJson["totalItem"]
    );
  }
}