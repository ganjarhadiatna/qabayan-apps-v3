import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
// import 'package:qabayan_ui/posts/index.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';

class CardBergainsWidget extends StatelessWidget {

  final val;

  CardBergainsWidget({Key key, @required this.val}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(top: 15),
      children: <Widget>[
        _onTitle('Hari ini'),
        _onCard(context, 0),
        _onCard(context, 0),

        _onTitle('Kemarin'),
        _onCard(context, 0),
      ],
    );
  }

  _onCard (BuildContext context, val) => new Card(
    semanticContainer: true,
    clipBehavior: Clip.antiAliasWithSaveLayer,
    color: Colors.white,
    elevation: 0.0,
    margin: EdgeInsets.only(bottom: 15, top: 0, left: 0, right: 0),
    child: new InkWell(
      onTap: () {
        Navigator.of(context).pushNamed(this.val);
      },
      onLongPress: () {
        _onMore(context);
      },
      child: new Container(
        padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[

                new Container(
                  margin: EdgeInsets.only(right: 10),
                  width: 50,
                  height: 50,
                  color: OnColors.whiteGrey,
                  child: ClipRRect(
                    borderRadius: new BorderRadius.circular(10),
                    child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                  ),
                ),

                new Expanded(
                  child: new Container (
                    width: double.infinity,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Expanded(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    'Cabai Rawit Merah',
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey.shade800,
                                    ),
                                    softWrap: true,
                                  ),
                                  new SizedBox(height: 2.5),
                                  new Text(
                                    'Dijual oleh green tea',
                                    style: TextStyle(color: OnColors.lightGrey, fontSize: 14),
                                  ),
                                ],
                              ),
                            ),
                            new Container(
                              // width: 40,
                              padding: EdgeInsets.only(left: 7.5, right: 7.5),
                              height: 25,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: OnColors.secondary
                              ),
                              child: new Center(
                                child: new Text(
                                  'Masih Dijual',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 13,
                                  )
                                )
                              ),
                            )
                          ]
                        ),
                        
                        new SizedBox(height: 5),
                        _onInfo('Harga sayuran', 'Rp 18.000', OnColors.black),
                        new SizedBox(height: 3),
                        _onInfo('Negosiasi terakhir', 'Rp 13.000', OnColors.tertiary),

                      ]
                    )
                  ),
                )

              ],
            ),

          ],
        )
      ),
    ),
  );

  _onTitle (var title) {
    return Container (
      margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
      child: Text(
        title,
        style: TextStyle(
          color: OnColors.black,
          fontSize: 16,
          fontWeight: FontWeight.normal,
        )
      ),
    );
  }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Nego',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.search_plus,
              type: 'white',
              title: 'Lihat detail nego'
            ),
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.trash,
              type: 'white',
              title: 'Hapus nego'
            ),
            new SizedBox(height: 15),
            OnLine(),
            new SizedBox(height: 15),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

  _onInfo (var title, var value, var valColor) {
    return new Container(
      width: double.infinity,
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[

          new Text(
            title,
            style: TextStyle(
              color: Colors.grey.shade800,
              fontWeight: FontWeight.normal,
              fontSize: 16
            ),
          ),

          new Text(
            value,
            style: TextStyle(
              color: valColor,
              fontWeight: FontWeight.bold,
              fontSize: 16
            ),
          ),

        ],
      ),
    );
  }
}