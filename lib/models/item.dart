class ItemList {
  final List<Item> items;
  ItemList({
    this.items
  });
  factory ItemList.fromJson(List<dynamic> parsedJson) {
    List<Item> items = new List<Item>();
    items = parsedJson.map((i) => Item.fromJson(i)).toList();
    return new ItemList(
      items: items
    );
  }
}

class Item {
  final String id;
  final String image;
  final String title;
  final String description;
  final String price;
  final String secondPrice;
  final String status;
  final String category;
  final String location;
  Item({
    this.id,
    this.image,
    this.title,
    this.description,
    this.price,
    this.secondPrice,
    this.status,
    this.category,
    this.location
  });
  factory Item.fromJson(Map<String, dynamic> parsedJson) {
    return Item(
      id: parsedJson["id"].toString(),
      image: parsedJson["image"],
      title: parsedJson["title"],
      description: parsedJson["description"],
      price: parsedJson["price"],
      secondPrice: parsedJson["secondPrice"],
      status: parsedJson["status"],
      category: parsedJson["category"],
      location: parsedJson["location"]
    );
  }
}