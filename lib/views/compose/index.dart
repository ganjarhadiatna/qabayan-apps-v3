import 'dart:io';
import 'dart:async';
import 'package:qabayan_ui/views/auth/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:image_picker/image_picker.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/fieldPlusMinus.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/popupAlert.dart';
import 'package:qabayan_ui/assets/popupQuestion.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';

class ComposePage extends StatefulWidget {
  @override
  _ComposePageState createState() => _ComposePageState();
}

class _ComposePageState extends State<ComposePage> {
  
  bool _image = false;
  File _imageUtama, _imageDepan, _imageAtas, _imageBawah;
  String _id;

  Future getUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString('id');
    setState(() {
      this._id = id;
    });
  }

  Future getImage(bool type, var path) async {
    var image;
    if (type) {
      image = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      image = await ImagePicker.pickImage(source: ImageSource.gallery);
    }

    setState(() {
      _image = true;
      switch (path) {
        case 'depan':
          _imageDepan = image;
          break;
        case 'atas':
          _imageAtas = image;
          break;
        case 'bawah':
          _imageBawah = image;
          break;
        default:
          _imageUtama = image;
          break;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    this.getUserID();
  }

  @override
  Widget build(BuildContext context) {
    return (this._id == null) ? LoginPage() : _onLayout();
  }

  _onLayout() {
    return new Scaffold(
      backgroundColor: OnColors.whiteGrey,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Tambah Sayuran'),
                    
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: Icons.more_vert,
                            action: () {
                              _onMore(context);
                            },
                          ),

                        ],
                      ),
                    )

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new Container(
        color: Colors.grey.shade100,
        child: new ListView(
          children: <Widget>[
            
            // foto
            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 15),
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: _onLabel('Masukan foto sayuran'),
            ),
            OnLine(),
            new Container(
              margin: EdgeInsets.only(bottom: 15),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
              color: Colors.white,
              width: double.infinity,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(child: _onAddPictureUtama(context)),
                        new Expanded(child: _onAddPictureDepan(context)),
                        new Expanded(child: _onAddPictureAtas(context)),
                        new Expanded(child: _onAddPictureBawah(context))
                      ],
                    )
                  )
                ]
              )
            ),

            // informasi
            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: _onLabel('Informasi sayuran'),
            ),
            OnLine(),
            new Container(
              margin: EdgeInsets.only(bottom: 15),
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              color: Colors.white,
              width: double.infinity,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _onTitle('Nama sayuran'),
                  new Container(
                    padding: EdgeInsets.only(top: 5, bottom: 15),
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      autofocus: false,
                      decoration: InputDecoration(
                        hintText: '',
                        // border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                        border:  OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                  ),

                  _onTitle('Deskripsi sayuran'),
                  new Container(
                    padding: EdgeInsets.only(top: 5, bottom: 15),
                    child: TextFormField(
                      keyboardType: TextInputType.multiline,
                      autofocus: false,
                      decoration: InputDecoration(
                        hintText: '',
                        // border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                        border:  OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                  ),

                  _onTitle('Stok / jumlah sayuran (Kg)'),
                  new Container(
                    padding: EdgeInsets.only(top: 5, bottom: 15),
                    child: FieldPlusMinus(),
                  ),

                  _onTitle('Lokasi penjualan'),
                  new Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: TextFormField(
                      keyboardType: TextInputType.multiline,
                      autofocus: false,
                      decoration: InputDecoration(
                        hintText: '',
                        // border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                        border:  OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                  ),

                ]
              )
            ),

            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: _onLabel('Harga sayuran (Kg)'),
            ),
            OnLine(),
            new Container(
              margin: EdgeInsets.only(bottom: 15),
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              color: Colors.white,
              width: double.infinity,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _onTitle('Harga jual pertama'),
                  new Container(
                    padding: EdgeInsets.only(top: 5, bottom: 15),
                    child: FieldPlusMinus(),
                  ),

                  _onTitle('Harga jual kedua'),
                  new Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: FieldPlusMinus(),
                  ),

                ]
              )
            ),

            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: _onLabel('Transaksi'),
            ),
            OnLine(),
            _onList('Pembayaran', 'pilih beberapa metode.', () {
              Navigator.of(context).pushNamed('compose-payment');
            }),
            OnLine(),
            _onList('Pengiriman', 'pilih beberapa metode.', () {
              Navigator.of(context).pushNamed('compose-delivery');
            }),
            new SizedBox(height: 15),

            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: _onLabel('Lainnya'),
            ),
            OnLine(),
            _onList('Kategori sayuran', 'pilih satu kategori sayuran.', () {
              Navigator.of(context).pushNamed('compose-category');
            }),
            OnLine(),
            _onList('Tipe penjualan', 'pilih satu tipe sayuran yang akan dijual.', () {
              Navigator.of(context).pushNamed('compose-type');
            }),
            // OnLine(),
            // _onList('Satuan sayuran', 'pilih satu satuan sayuran.', () {
            //   Navigator.of(context).pushNamed('compose-unit');
            // }),
            new SizedBox(height: 15),

          ],
        ),
      ),

      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        elevation: 0.0,
        clipBehavior: Clip.none,
        child: new Container(
          width: double.infinity,
          padding: EdgeInsets.all(15),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Expanded(
                child: ButtonText(
                  title: 'Batalkan',
                  type: 'grey',
                  action: () {
                    _onDiscard(context);
                  },
                ),
              ),
              new SizedBox(width: 15),
              new Expanded(
                child: ButtonText(
                  title: 'Tambahkan Sayuran',
                  type: 'main',
                  action: () {
                    _onSales(context);
                  },
                ),
              )
            ],
          ),
        )
      )

    );
  }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Tambahkan Sayuran',
        content: new Column(
          children: <Widget>[
            ButtonText(
              action: () {
                _onSales(context);
              },
              type: 'main',
              title: 'Tambahkan Sayuran'
            ),
            new SizedBox(height: 10),
            ButtonText(
              action: () {
                _onSales(context);
              },
              type: 'grey',
              title: 'Simpan dulu di Draft'
            ),
            new Container(
              padding: EdgeInsets.only(top: 15, bottom: 15),
              child: OnLine(),
            ),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

  _onDiscard (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Batalkan',
        content: new Column(
          children: <Widget>[
            ButtonText(
              action: () {
                Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => false);
              },
              type: 'red',
              title: 'Batalkan Perubahan'
            ),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'white',
              title: 'Lanjutkan'
            ),
          ]
        ),
      ),
    );
  }

  _onChooseImage (BuildContext context, var path) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Foto sayuran',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              title: 'Langsung dari kamera', 
              icon: LineIcons.camera, 
              type: 'white', 
              action: () {
                getImage(true, path);
                Navigator.pop(context);
              }
            ),
            ButtonTextIcon(
              title: 'Pilih dari galeri', 
              icon: LineIcons.image, 
              type: 'white', 
              action: () {
                getImage(false, path);
                Navigator.pop(context);
              }
            ),
            new Container(
              margin: EdgeInsets.only(top: 15, bottom: 15),
              child: OnLine(),
            ),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

  _onSales (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupQuestion(
        title: 'Konfirmasi',
        subtitle: 'Jika data sudah benar, jual sayuran sekarang?',
        action: () {
          Navigator.pop(context);
          showDialog(
            context: context,
            builder: (BuildContext context) => PopupAlert(
              title: 'Pemberitahuan',
              subtitle: 'Sayuran kamu berhasil dipublikasi.',
              action: () {
                Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => false);
              },
            )
          );
          // Navigator.pop(context);
          // showDialog(
          //   context: context,
          //   builder: (BuildContext context) => PopupLoading(
          //     title: 'sayuran sedang di upload..',
          //     action: () {
          //       Navigator.pop(context);
          //       showDialog(
          //         context: context,
          //         builder: (BuildContext context) => PopupAlert(
          //           title: 'Pemberitahuan',
          //           subtitle: 'Sayuran kamu berhasil dipublikasi.',
          //           action: () {
          //             Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => false);
          //           },
          //         )
          //       );
          //     }
          //   )
          // );
        }
      )
    );
  }

  _onContainerPicture (var title, File image) {
    return new Container(
      child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Container(
              width: 85,
              height: 85,
              margin: EdgeInsets.all(5),
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.grey.shade100,
                borderRadius: BorderRadius.circular(5),
              ),
              child: new DottedBorder(
                color: OnColors.grey,
                strokeWidth: 1,
                borderType: BorderType.RRect,
                radius: Radius.circular(10),
                child: image == null
                  ? new Center(
                    child: new Icon(
                        LineIcons.plus,
                        color: OnColors.grey,
                        size: 32,
                    )
                  )
                  : new ClipRRect(
                    borderRadius: new BorderRadius.circular(10),
                    child: new Container(
                      width: double.infinity,
                      height: double.infinity,
                      child: Image.file(
                        image, 
                        fit: BoxFit.cover
                      )
                    )
                  )
              ),
            ),
            new SizedBox(height: 5),
            _onTitle(title),
          ],
        ),
    );
  }

  _onAddPictureUtama (BuildContext context) {
    return new InkWell(
      onTap: () {
        _onChooseImage(context, 'utama');
      },
      child: _onContainerPicture('Utama', _imageUtama)
    );
  }

  _onAddPictureDepan (BuildContext context) {
    return new InkWell(
      onTap: () {
        _onChooseImage(context, 'depan');
      },
      child: _onContainerPicture('Depan', _imageDepan)
    );
  }

  _onAddPictureAtas (BuildContext context) {
    return new InkWell(
      onTap: () {
        _onChooseImage(context, 'atas');
      },
      child: _onContainerPicture('Atas', _imageAtas)
    );
  }

  _onAddPictureBawah (BuildContext context) {
    return new InkWell(
      onTap: () {
        _onChooseImage(context, 'bawah');
      },
      child: _onContainerPicture('Bawah', _imageBawah)
    );
  }

  _onList (var title, var subtitle, var action) {
    return new InkWell(
      onTap: action,
      child: new Container(
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[

            new Expanded(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _onLabel(title),
                  new SizedBox(height: 2.5),
                  Text(
                    subtitle,
                    style: TextStyle(
                      color: OnColors.grey,
                      fontSize: 14,
                      fontWeight: FontWeight.normal
                    )
                  )
                ],
              ),
            ),

            ButtonCircle(
              action: () {
                // Navigator.of(context).push(
                //   MaterialPageRoute(
                //     builder: (context) => DeliveryMethodPage()
                //   )
                // );
              },
              icon: Icons.chevron_right,
              size: 32.0,
              type: 'white',
            ),

          ],
        ),
      ),
    );
  }

  _onLabel (var title) {
    return new Text(
      title,
      style: TextStyle(
        color: OnColors.black,
        fontSize: 16,
        fontWeight: FontWeight.bold
      ),
    );
  }

  _onTitle (var title) {
    return Container (
      child: Text(
        title,
        style: TextStyle(
          color: OnColors.black,
          fontSize: 16,
          fontWeight: FontWeight.normal
        )
      ),
    );
  }
}