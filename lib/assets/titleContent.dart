import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class TitleContent extends StatelessWidget {
  final title;
  final subtitle;
  final more;
  final action;

  TitleContent({
    Key key,
    @required this.title, 
    this.subtitle, 
    this.more, 
    this.action
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    var sub;
    var mor;

    if (this.subtitle != '') {
      sub = new Container(
        margin: EdgeInsets.only(top: 2.5),
        child: new Text(
          this.subtitle,
          style: TextStyle(
            color: Colors.grey.shade600,
            fontSize: 16,
            fontWeight: FontWeight.normal
          ),
        ),
      );
    } else {
      sub = new SizedBox();
    }

    if (this.more != '') {
      mor = new InkWell(
        child: Text(
          this.more, 
          style: TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 16,
            color: OnColors.main
          ),
          softWrap: true
        ),
        onTap: action,
      );
    } else {
      mor = new SizedBox();
    }

    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  this.title, 
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    color: OnColors.black
                  ),
                  softWrap: true
                ),

                // subtitle
                sub
                
              ],
            ),
          ),

          // more
          mor
          
        ],
      ),
    );

  }
}