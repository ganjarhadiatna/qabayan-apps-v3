import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
// import 'package:qabayan_ui/posts/index.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';

class CardChatHistoryWidget extends StatelessWidget {

  final int val;

  CardChatHistoryWidget({Key key, @required this.val}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        new SizedBox(height: 15),
        _onCardLeft(context, this.val),
        _onCardLeft(context, this.val),
        _onCardRight(context, this.val),
        _onCardLeft(context, this.val),
        _onCardRight(context, this.val),
        _onCardRight(context, this.val),
      ],
    );
  }


  _onCardLeft (BuildContext context, val) {
    return new InkWell(
      onLongPress: () {
        _onMore(context);
      },
      child: new Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(20, 0, 20, 15),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              margin: EdgeInsets.only(bottom: 5),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                )
              ),
              child: new Text(
                'Lorem ipsum..',
                style: TextStyle(color: OnColors.black, fontSize: 16),
              ),
            ),

            new Container(
              child: new Text(
                '5 menit yang lalu',
                style: TextStyle(color: OnColors.lightGrey, fontSize: 14),
              ),
            )

          ],
        ),
      )
    );
  }

  _onCardRight (BuildContext context, val) {
    return new InkWell(
      onLongPress: () {
        _onMore(context);
      },
      child: new Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(20, 0, 20, 15),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            new Container(
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              margin: EdgeInsets.only(bottom: 5),
              decoration: BoxDecoration(
                color: OnColors.main,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                )
              ),
              child: new Text(
                'Lorem ipsum, dolor sit amet..',
                textAlign: TextAlign.end,
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),

            new Container(
              child: new Text(
                '5 menit yang lalu',
                style: TextStyle(color: OnColors.lightGrey, fontSize: 14),
              ),
            )

          ],
        )
      )
    );
  }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Pengaturan',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.pencil,
              type: 'white',
              title: 'Edit pesan'
            ),
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.trash,
              type: 'white',
              title: 'Hapus pesan'
            ),
            new SizedBox(height: 15),
            OnLine(),
            new SizedBox(height: 15),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

}