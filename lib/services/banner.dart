import 'dart:async' show Future;
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:qabayan_ui/models/banner.dart';

Future<String> _loadBanner() async {
  return await rootBundle.loadString('assets/jsons/banner.json');
}

Future loadBanner() async {
  String jsonString = await _loadBanner();
  final jsonResponse = json.decode(jsonString);
  BannerList banner = new BannerList.fromJson(jsonResponse);
  return banner.banners;
}