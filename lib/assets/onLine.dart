import 'package:flutter/material.dart';

class OnLine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 0.5,
      color: Colors.grey.shade400,
    );
  }
}