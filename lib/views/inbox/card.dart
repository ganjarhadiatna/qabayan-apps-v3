import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
// import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class CardActivitesWidget extends StatelessWidget {

  final cardTitle;
  final cardType;

  CardActivitesWidget({
    Key key,
    this.cardTitle,
    this.cardType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _onCardNormal(context, this.cardTitle, this.cardType);
  }

  _onCardNormal (BuildContext context, var title, var type) {

    Widget onBottom;
    Widget onValue;

    if (type == 'offers') {
      onValue = new Column(
        children: <Widget>[
          new Row(
            children: <Widget>[
              Text(
                'Harga sayuran: ',
                style: TextStyle(
                  color: Colors.grey.shade800,
                  fontSize: 16,
                  fontWeight: FontWeight.bold
                ),
              ),
              Text(
                'Rp 15.000',
                style: TextStyle(
                  color: OnColors.tertiary,
                  fontSize: 16,
                  fontWeight: FontWeight.bold
                ),
              )
            ],
          ),

          new SizedBox(height: 5),

          new Row(
            children: <Widget>[
              Text(
                'Negosiasi: ',
                style: TextStyle(
                  color: Colors.grey.shade800,
                  fontSize: 16,
                  fontWeight: FontWeight.bold
                ),
              ),
              Text(
                'Rp 13.000',
                style: TextStyle(
                  color: OnColors.tertiary,
                  fontSize: 16,
                  fontWeight: FontWeight.bold
                ),
              )
            ],
          )
        ],
      );

      onBottom = new Container(
        // margin: EdgeInsets.only(top: 10),
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: double.infinity
          ),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              // Container(
              //   padding: EdgeInsets.only(top: 10, bottom: 10),
              //   child: OnLine()
              // ),

              // new Container(
              //   margin: EdgeInsets.only(bottom: 10, top: 10),
              //   child: Text(
              //     'Setuju dengan penawaran ini?',
              //     style: TextStyle(
              //       color: Colors.grey.shade800,
              //       fontSize: 16
              //     ),
              //   ),
              // ),

              new SizedBox(height: 10),

              ButtonText(
                title: 'Lihat Selengkapnya',
                type: 'grey',
                action: () {
                  Navigator.of(context).pushNamed('bergains-sales');
                },
              ),

              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   children: <Widget>[

              //     new Expanded(
              //       child: ButtonText(
              //         title: 'Tidak',
              //         type: 'grey',
              //         action: () {},
              //       ),
              //     ),

              //     new SizedBox(width: 10),

              //     new Expanded(
              //       child: ButtonText(
              //         title: 'Ya',
              //         type: 'main',
              //         action: () {},
              //       ),
              //     ),

              //     // RaisedButton(
              //     //     onPressed: () {},
              //     //     shape: RoundedRectangleBorder(
              //     //       borderRadius: BorderRadius.circular(5)
              //     //     ),
              //     //     elevation: 0,
              //     //     disabledElevation: 1,
              //     //     highlightElevation: 0,
              //     //     child: Text(
              //     //       'Tidak', 
              //     //       style: TextStyle(
              //     //         color: Colors.grey.shade800
              //     //       )
              //     //     ),
              //     //     color: Colors.grey.shade100,
              //     // ),

              //     // RaisedButton(
              //     //     onPressed: () {},
              //     //     shape: RoundedRectangleBorder(
              //     //       borderRadius: BorderRadius.circular(5)
              //     //     ),
              //     //     elevation: 0,
              //     //     disabledElevation: 1,
              //     //     highlightElevation: 0,
              //     //     child: Text(
              //     //       'Ya', 
              //     //       style: TextStyle(
              //     //         color: Colors.white
              //     //       )
              //     //     ),
              //     //     color: OnColors.main,
              //     // ),

              //   ],
              // )

            ],
          ),
        ),
      );
    }

    if (type == 'comments') {
      
      onValue = Text(
        'Lorem ipsum doler ismet..',
        style: TextStyle(
          color: Colors.grey.shade800,
          fontSize: 16,
          fontWeight: FontWeight.bold
        ),
      );

      onBottom = new Container(
        padding: EdgeInsets.only(top: 10),
        child: ButtonText(
          title: 'Lihat Selengkapnya',
          type: 'grey',
          action: () {
            Navigator.of(context).pushNamed('chats-history');
          },
        ),
      );
    }

    if (type == 'detail') {
      onValue = Container();
      onBottom = new Container(
        padding: EdgeInsets.only(top: 0),
        child: ButtonText(
          title: 'Lihat Sayuran',
          type: 'grey',
          action: () {
            Navigator.of(context).pushNamed('detail-post');
          },
        ),
      );
    }

    if (type == 'detail-order') {
      onValue = Container();
      onBottom = Container(
        // margin: EdgeInsets.only(top: 5),
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: double.infinity
          ),
          child: RaisedButton(
            onPressed: () {
              Navigator.of(context).pushNamed('post-view');
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5)
            ),
            elevation: 0,
            disabledElevation: 1,
            highlightElevation: 0,
            child: Text(
              'Lihat Detail Sayuran', 
              style: TextStyle(
                color: Colors.grey.shade800
              )
            ),
            color: Colors.grey.shade100,
          ),
        ),
      );
    }
    
    if (type == '') {
      onValue = Container();
      onBottom = Container();
    }

    return new Card (
      semanticContainer: true,
      elevation: 0.0,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0)
      ),
      margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
      child: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,

          children: <Widget>[

            new Container(
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  new Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new InkWell(
                          onTap: () {
                            Navigator.of(context).pushNamed('user-profile');
                          },
                          child: new Container(
                            margin: EdgeInsets.only(right: 10),
                            width: 55,
                            height: 55,
                            color: OnColors.whiteGrey,
                            child: ClipRRect(
                              borderRadius: new BorderRadius.circular(55),
                              // child: Image.asset('assets/person/1.jpg', fit: BoxFit.cover),
                            ),
                          )
                        ),
                      ],
                    ),
                  ),

                  new Expanded(
                    child: new ConstrainedBox(
                      constraints: BoxConstraints(minWidth: double.infinity),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          textDirection: TextDirection.ltr,
                          verticalDirection: VerticalDirection.down,
                          children: <Widget>[

                            new InkWell(
                              onTap: () {
                                Navigator.of(context).pushNamed('user-profile');
                              },
                              child: new Text(
                                'Green Tea',
                                style: TextStyle(
                                  color: Colors.grey.shade800,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                            ),

                            new SizedBox(height: 2.5),

                            new Text(
                              title,
                              style: TextStyle(
                                color: Colors.grey.shade800,
                                fontSize: 16,
                                fontWeight: FontWeight.normal
                              ),
                            ),

                            new SizedBox(height: 5),

                            new Text(
                              '15 menit yang lalu',
                              style: TextStyle(
                                color: Colors.grey.shade500,
                                fontSize: 14,
                                fontWeight: FontWeight.normal
                              ),
                            ),

                            new SizedBox(height: 10),
                            onValue,
                            onBottom,
                          ],
                        ),
                      )
                  ),


                ],
              ),
            ),
          ]  

      )
    );
  }

}