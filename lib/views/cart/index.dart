import 'dart:async';
import 'package:qabayan_ui/views/auth/login.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
// import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
// import 'package:qabayan_ui/views/cart/card.dart';
import 'package:qabayan_ui/views/cart/listBorongan.dart';
import 'package:qabayan_ui/views/cart/listKiloan.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  String _id;

  Future getUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString('id');
    setState(() {
      this._id = id;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getUserID();
  }

  @override
  Widget build(BuildContext context) {
    return (this._id == null) ? LoginPage() : _onLayout();
  }

  _onLayout() {
    return new Scaffold(
      backgroundColor: OnColors.whiteGrey,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Keranjang'),

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: new PreferredSize(
            preferredSize: Size(double.infinity, 60),
            child: new Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: new TabBar(
                unselectedLabelColor: Colors.grey.shade600,
                labelColor: Colors.white,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: OnColors.main,
                  border: Border.all(
                    width: 1,
                    color: OnColors.main
                  )
                ),
                tabs: <Widget>[
                  new Tab(
                    child: new Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Kiloan'),
                      ),
                    ),
                  ),
                  new Tab(
                    child: new Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Borongan'),
                      ),
                    ),
                  ),
                ]
              ),
            )
          ),
          body: new Container(
            color: OnColors.whiteGrey,
            child: new TabBarView(
              children: <Widget>[

                ListKiloanPage(),
                ListBoronganPage()

              ],
            ),
          ),
        ),
      )
    );
  }
}