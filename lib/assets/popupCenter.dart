import 'package:flutter/material.dart';

class PopupCenter extends StatelessWidget {

  final String title;
  final Widget content;

  PopupCenter({
    Key key,
    @required this.title,
    @required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
      alignment: Alignment(0,0),
      child: new Container(
        width: double.infinity,
        padding: EdgeInsets.all(20),
        margin: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20)
        ),
        child: new Material(
          color: Colors.transparent,
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              new Text(
                this.title,
                style: TextStyle(
                  color: Colors.grey.shade800,
                  fontSize: 24,
                  fontWeight: FontWeight.bold
                ),
              ),

              SizedBox(height: 20),

              new Container(
                child: this.content,
              )

            ],
          ),
        ),
      ),
    );
  }
}