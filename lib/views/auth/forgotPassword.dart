import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
// import 'package:qabayan_ui/assets/onColors.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Lupa Pasword'),

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new ListView(
        children: <Widget>[

          new SizedBox(height: 30),

          new Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: new Text(
              'Lupa Password?',
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.grey.shade800,
                fontWeight: FontWeight.bold,
                fontSize: 56,
              ),
            ),
          ),

          new Container(
            margin: EdgeInsets.only(left: 20, right: 20, top: 5),
            child: new Text(
              'Jangan khawatir ..',
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.grey.shade600,
                fontWeight: FontWeight.normal,
                fontSize: 16,
              ),
            ),
          ),
          
          new SizedBox(height: 30),

          // email
          new Container(
            margin: EdgeInsets.only(bottom: 5, left: 20, right: 20),
            child: _onTitleField('Masukan alamat e-mail akun kamu'),
          ),
          new Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: new TextFormField(
              keyboardType: TextInputType.emailAddress,
              autofocus: false,
              decoration: new InputDecoration(
                hintText: 'alamat-email@qabayan.com',
                contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
                border:  OutlineInputBorder(
                  borderRadius: BorderRadius.circular(7.5),
                ),
              )
            ),
          ),

          new SizedBox(height: 15),

          // password
          new Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: new Text(
              'Kami akan mengirimkan link ke email yang di submit untuk perubahan password kamu, link ini akan kadaluarsa setelah 24 jam dari mulai pengiriman.',
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.grey.shade600,
                fontSize: 16,
              ),
            ),
          ),

          new SizedBox(height: 15),
        ],
      ),

      bottomNavigationBar: BottomAppBar(
        elevation: 0.0,
        child: new Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
          width: double.infinity,
          child: ButtonText(
            action: () {
              Navigator.of(context).pushNamed('home');
            },
            type: 'main',
            title: 'Submit',
          ),
        )
      )
    );
  }

  _onTitleField (title) {
    return Container(
      child: new Text(
        title, 
        style: TextStyle(
          fontWeight: FontWeight.normal,
          fontSize: 14.0,
          color: Colors.grey.shade800
        ),
        softWrap: true
      ),
    );
  }
}