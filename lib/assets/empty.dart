import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class EmptyPage extends StatelessWidget {
  final title;
  final icon;

  EmptyPage({
    Key key,
    @required this.title,
    @required this.icon,
  }) : super (key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey.shade100,
      child: ListView(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 100, bottom: 20),
                width: 130,
                height: 130,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.grey.shade200,
                ),
                child: Icon(
                  icon,
                  color: OnColors.main,
                  size: 50.0,
                ),
              ),
              Container(
                width: 250,
                margin: EdgeInsets.only(bottom: 100),
                child: Center(
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      )
    );
  }
}