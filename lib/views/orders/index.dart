import 'dart:io';
import 'dart:async';
import 'package:qabayan_ui/views/auth/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/fieldPlusMinus.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/popupAlert.dart';
import 'package:qabayan_ui/assets/popupLoading.dart';
import 'package:qabayan_ui/assets/popupQuestion.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/views/orders/paymentMethod.dart';
import 'package:qabayan_ui/views/orders/deliveryMethod.dart';

class MakeOrderPage extends StatefulWidget {
  @override
  _MakeOrderPageState createState() => _MakeOrderPageState();
}

class _MakeOrderPageState extends State<MakeOrderPage> {
  String _id;

  Future getUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString('id');
    setState(() {
      this._id = id;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getUserID();
  }

  @override
  Widget build(BuildContext context) {
    return (this._id == null) ? LoginPage() : _onLayout();
  }

  _onLayout() {
    return Scaffold(
      
      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Buat Pesanan'),
                    
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: Icons.more_vert,
                            action: () {
                              _onMore(context);
                            },
                          ),

                        ],
                      ),
                    )

                  ],
                )
              )
            )

          ],
        ),
      ),
      body: new Container(
        color: Colors.grey.shade100,
        child: new ListView(
          children: <Widget>[

            new SizedBox(height: 15),

            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed('detail-post');
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 35,
                      height: 35,
                      color: OnColors.whiteGrey,
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(5),
                        // child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  new Container(
                    child: new Text(
                      'Green Tea',
                      style: TextStyle(
                        color: OnColors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal
                      )
                    ),
                  ),
                  new Expanded(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        ButtonCircle(
                          action: () {
                            Navigator.of(context).pushNamed('chats-history');
                          },
                          icon: LineIcons.comments,
                          size: 22.0,
                          type: 'white',
                        ),
                        ButtonCircle(
                          action: () {
                            Navigator.of(context).pushNamed('detail-post');
                          },
                          icon: Icons.chevron_right,
                          size: 28.0,
                          type: 'white',
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            
            OnLine(),

            new Container(
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              color: Colors.white,
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[

                  new InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed('detail-post');
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 70,
                      height: 70,
                      color: OnColors.whiteGrey,
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(5),
                        child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                      ),
                    ),
                  ),

                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          'Cabai rawit merah',
                          style: TextStyle(
                            color: Colors.grey.shade800,
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        new SizedBox(height: 5),
                        _onInfo ('Stok', 'Sisa 30 Kg', OnColors.black),
                        _onInfo ('Harga', 'Rp 13.000', OnColors.tertiary),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            // new Container(
            //   color: Colors.white,
            //   padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
            //   child: _cardMainTitile('Jumlah pembelian'),
            // ),
            new Container(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 15),
              color: Colors.white,
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Expanded(
                    child: _onSubtitle('Masukan sayuran yang ingin dipesan dalam kilogram (Kg).'),
                  ),
                  new SizedBox(height: 10),
                  new Container(
                    width: 150,
                    child: FieldPlusMinus(),
                  ),

                ],
              ),
            ),
            OnLine(),
            _onList('Pembayaran sayuran', 'pilih satu metode.', () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => PaymentMethodPage()
                )
              );
            }),
            OnLine(),
            _onList('Pengiriman sayuran', 'pilih satu metode.', () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => DeliveryMethodPage()
                )
              );
            }),

            SizedBox(height: 15),
            
            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: _onLabel('Catatan'),
            ),
            OnLine(),
            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(20, 15, 20, 0),
              child: _cardMainTitile('Catatan atau pesan untuk penjual'),
            ),
            new Container(
              padding: EdgeInsets.fromLTRB(20, 5, 20, 20),
              color: Colors.white,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                    keyboardType: TextInputType.multiline,
                    autofocus: false,
                    decoration: InputDecoration(
                      hintText: '',
                      // border: InputBorder.none,
                      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
                      border:  OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                  ),

                ],
              ),
            ),

            SizedBox(height: 15),

            new Container(
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              color: Colors.white,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _onSubtitle("* Silahkan periksa kembali data pesanan kamu sebelum melakukan pesanan."),
                  new SizedBox(height: 5),
                  _onSubtitle("* Pembatalan pesanan hanya bisa dilakukan oleh kedua belah pihak saja."),
                ]
              )
            ),

            SizedBox(height: 15),

          ],
        ),
      ),

      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        elevation: 0.0,
        clipBehavior: Clip.none,
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              new Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    _onLabel('Jumlah pembayaran'),
                    new SizedBox(height: 2.5),
                    new Text(
                      'Rp 0',
                      style: TextStyle(
                        color: OnColors.tertiary,
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                      ),
                    ),
                  ],
                ),
              ),

              new Container(
                width: 150,
                child: ButtonText(
                  action: () {
                    // Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => false);
                    return _onOrder(context);
                  },
                  type: 'main',
                  title: 'Buat Pesanan',
                ),
              )

            ],
          )
        )
      ),
    );
  }

  _onInfo (var title, var value, var valColor) {
    return new Container(
      margin: EdgeInsets.only(bottom: 2.5, top: 2.5),
      child: new Row(
        children: <Widget>[

          new Expanded(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Text(
                  title,
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontWeight: FontWeight.normal,
                    fontSize: 16
                  ),
                )
              ],
            ),
          ),

          new Expanded(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new Text(
                  value,
                  style: TextStyle(
                    color: valColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16
                  ),
                )
              ],
            ),
          ),

        ],
      ),
    );
  }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupLoading(title: 'Mohon tunggu sebentar..')
      // builder: (BuildContext context) => PopupBottom(
      //   title: 'Beli Barang',
      //   content: new Column(
      //     children: <Widget>[
      //       ButtonText(
      //         action: () {},
      //         type: 'primary',
      //         title: 'Buat Pesanan'
      //       ),
      //       new SizedBox(height: 15,),
      //       ButtonText(
      //         action: () {
      //           Navigator.pop(context);
      //         },
      //         type: 'grey',
      //         title: 'Tutup'
      //       ),
      //     ]
      //   ),
      // ),
    );
  }

  _onOrder (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupQuestion(
        title: 'Konfirmasi',
        subtitle: 'Buat pesanan ini sekarang?',
        action: () {
          Navigator.pop(context);
          showDialog(
            context: context,
            builder: (BuildContext context) => PopupAlert(
              title: 'Pemberitahuan',
              subtitle: 'Pesanan kamu berhasil dibuat.',
              action: () {
                Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => false);
              },
            )
          );
        }
      )
    );
  }

  _cardMainTitile (var title) {
    return Text(
      title, 
      style: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 16,
        color: OnColors.black
      ),
      softWrap: true
    );
  }

  // _onLabel (var title) {
  //   return Text(
  //     title,
  //     style: TextStyle(
  //       color: Colors.grey.shade600,
  //       fontWeight: FontWeight.normal,
  //       fontSize: 14.0,
  //     ),
  //   );
  // }

  _onList (var title, var subtitle, var action) {
    return new InkWell(
      onTap: action,
      child: new Container(
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[

            new Expanded(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _onTitle(title),
                  new SizedBox(height: 2.5),
                  _onSubtitle(subtitle),
                ],
              ),
            ),

            ButtonCircle(
              action: () {
                // Navigator.of(context).push(
                //   MaterialPageRoute(
                //     builder: (context) => DeliveryMethodPage()
                //   )
                // );
              },
              icon: Icons.chevron_right,
              size: 32.0,
              type: 'white',
            ),

          ],
        ),
      ),
    );
  }

  _onTitle (var title) {
    return Container (
      child: Text(
        title,
        style: TextStyle(
          color: OnColors.black,
          fontSize: 16,
          fontWeight: FontWeight.bold,
        )
      ),
    );
  }

  _onSubtitle (var title) {
    return new Text(
      title,
      style: TextStyle(
        color: OnColors.grey,
        fontSize: 14,
        fontWeight: FontWeight.normal,
      ),
    );
  }

  _onLabel (var title) {
    return new Text(
      title,
      style: TextStyle(
        color: OnColors.black,
        fontSize: 16,
        fontWeight: FontWeight.normal,
      ),
    );
  }

}