import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/titleContent.dart';
import 'package:qabayan_ui/assets/buttonText.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    
    return new Scaffold(
      body: new ListView(
        children: <Widget>[

          new SizedBox(height: 25),

          // info
          new Container(
            padding: EdgeInsets.only(left: 25, right: 25, top: 15, bottom: 15),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  'Qabayan',
                  style: new TextStyle(
                    color: OnColors.main,
                    fontWeight: FontWeight.bold,
                    fontSize: 26
                  ),
                ),
                new SizedBox(height: 10),
                new Text(
                  'Sampurasun',
                  style: new TextStyle(
                    color: OnColors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 48
                  ),
                ),
                new SizedBox(height: 10),
                new Text(
                  'Qabayan adalah tempat untuk jual dan beli sayuran dengan harga dan penawaran terbaik.',
                  style: new TextStyle(
                    color: OnColors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 16
                  ),
                ),
              ],
            ),
          ),

          // feature
          new Container(
            padding: EdgeInsets.only(left: 25, right: 25, top: 15, bottom: 15),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                
                new TitleContent(
                  title: 'Apa yang akan kamu dapatkan?',
                  subtitle: '',
                  more: '',
                  action: () {},
                ),

                new SizedBox(height: 15),

                _onCategory(Icons.add_shopping_cart, 'Kemudahan jual dan beli sayuran'),
                new SizedBox(height: 15),
                _onCategory(Icons.chat_bubble, 'Diskusi dengan pelanggan'),
                new SizedBox(height: 15),
                _onCategory(Icons.compare_arrows, 'Nego sampai harga terbaik'),
                new SizedBox(height: 15),
                _onCategory(Icons.group_add, 'Tawarkan sayuran langsung ke pembeli'),

              ],
            ),
          ),

          // join
          new Container(
            padding: EdgeInsets.only(left: 25, right: 25, top: 15, bottom: 15),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new TitleContent(
                  title: 'Bergabung dengan qabayan?',
                  subtitle: '',
                  more: '',
                  action: () {},
                ),

                new SizedBox(height: 15),

                ButtonText(
                  title: 'Daftar dulu dengan email',
                  type: 'primary',
                  action: () {},
                ),

                new SizedBox(height: 15),

                ButtonText(
                  title: 'Langsung login ke akun',
                  type: 'grey',
                  action: () {},
                ),

                new Container(
                  margin: EdgeInsets.all(15),
                  child: new Center(
                    child: new Text(
                      'atau langsung bergabung dengan',
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        color: Colors.grey.shade600,
                        fontSize: 15
                      )
                    ),
                  )
                ),

                new Container(
                  child: new Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Expanded(
                        child: ButtonText(
                          title: 'Facebook',
                          type: 'grey',
                          action: () {},
                        ),
                      ),
                      new SizedBox(width: 15),
                      new Expanded(
                        child: ButtonText(
                          title: 'Google',
                          type: 'grey',
                          action: () {},
                        ),
                      )
                    ],
                  ),
                )
                
              ],
            )
          ),

          // try
          new Container(
            padding: EdgeInsets.only(left: 25, right: 25, top: 15, bottom: 15),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new TitleContent(
                  title: 'Masih ragu?',
                  subtitle: 'Coba aja dulu, lihat dan eksplorasi sepuasnya.',
                  more: '',
                  action: () {},
                ),

                new SizedBox(height: 15),

                ButtonText(
                  title: 'Jelajahi qabayan?',
                  type: 'grey',
                  action: () {},
                ),
              ]
            )
          ),

          new SizedBox(height: 25),


        ],
      )
    );
  }

  _onCategory(var icon, var title) {
    return new Container(
      child: new Row(
        children: <Widget>[

          new Container(
            width: 50,
            height: 50,
            margin: EdgeInsets.only(right: 15),
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: OnColors.main
            ),
            child: new Icon(
              icon,
              color: Colors.white,
              size: 26,
            ),
          ),

          new Text(
            title,
            style: new TextStyle(
              color: OnColors.black,
              fontSize: 16
            ),
          )
        ],
      ),
    );
  }
}