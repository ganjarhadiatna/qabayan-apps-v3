// import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
import 'package:qabayan_ui/assets/fieldPlusMinus.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onColors.dart';
// import 'package:qabayan_ui/assets/popupAlert.dart';
// import 'package:qabayan_ui/assets/popupQuestion.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/popupAlert.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
import 'package:qabayan_ui/assets/popupQuestion.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
// import 'package:qabayan_ui/views/bergains/orders/card.dart';

class BergainOrderPage extends StatefulWidget {
  @override
  _BergainOrderPageState createState() => _BergainOrderPageState();
}

class _BergainOrderPageState extends State<BergainOrderPage> {

  String _status = 'empty';

  Future changeStatus(newStatus) async {
    setState(() {
      this._status = newStatus;
    });
  }

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Negosiasi Harga'),
                    
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Container(
                            // width: 40,
                            padding: EdgeInsets.only(left: 7.5, right: 7.5),
                            height: 25,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: OnColors.secondary
                            ),
                            child: new Center(
                              child: new Text(
                                'Masih Dijual',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 13,
                                )
                              )
                            ),
                          ),
                          new SizedBox(width: 5),
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: Icons.more_vert,
                            action: () {
                              _onMore(context);
                            },
                          ),

                        ],
                      ),
                    )

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new Container(
        color: Colors.grey.shade100,
        child: new ListView(
          children: <Widget>[
            new SizedBox(height: 15),

            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed('detail-post');
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 35,
                      height: 35,
                      color: OnColors.whiteGrey,
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(5),
                        child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  new Container(
                    child: new Text(
                      'Green Tea',
                      style: TextStyle(
                        color: OnColors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal
                      )
                    ),
                  ),
                  new Expanded(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        ButtonCircle(
                          action: () {
                            Navigator.of(context).pushNamed('chats-history');
                          },
                          icon: LineIcons.comments,
                          size: 22.0,
                          type: 'white',
                        ),
                        ButtonCircle(
                          action: () {
                            Navigator.of(context).pushNamed('detail-post');
                          },
                          icon: Icons.chevron_right,
                          size: 28.0,
                          type: 'white',
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),

            OnLine(),

            new Container(
              padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
              color: Colors.white,
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  new InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed('detail-post');
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 70,
                      height: 70,
                      color: OnColors.whiteGrey,
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(10),
                        child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          'Cabai rawit merah',
                          style: TextStyle(
                            color: Colors.grey.shade800,
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        new SizedBox(height: 5),
                        _onInfo ('Stok', 'Sisa 20 Kg', Colors.grey.shade800),
                        _onInfo ('Harga', 'Rp 18.000', OnColors.tertiary),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            // new SizedBox(height: 15),
            // OnLine(),

            new Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
              color: Colors.white,
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    child: new Text(
                      'Riwayat negosiasi',
                      style: TextStyle(
                        color: Colors.grey.shade800,
                        fontSize: 16,
                        fontWeight: FontWeight.normal
                      ),
                    )
                  ),
                  new SizedBox(height: 10),
                  _onHistory('Rp 10.000', 'Ditolak'),
                  new SizedBox(height: 10),
                  _onHistory('Rp 13.000', 'Ditolak')
                ]
              )
            ),

            new SizedBox(height: 15),

            new Container(
              child: (this._status == "empty") 
              ? _startBergain() 
              : (this._status == "waiting") 
              ? _waitingBergain()
              : (this._status == "decline") 
              ? _resultBergain("decline")
              : _resultBergain("done")
            )
            
          ]
        )
      )

    );
  }

  _sendBergain () {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupQuestion(
        title: 'Konfirmasi',
        subtitle: 'Kirim negosiasi harga?',
        action: () {
          Navigator.pop(context);
          changeStatus('waiting');
          // showDialog(
          //   context: context,
          //   builder: (BuildContext context) => PopupAlert(
          //     title: 'Pemberitahuan',
          //     subtitle: 'Negosiasi kamu berhasil dikirim.',
          //     action: () {
          //       Navigator.pop(context);
          //     },
          //   )
          // );
        }
      )
    );
  }

  _cancelBergain () {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupQuestion(
        title: 'Konfirmasi',
        subtitle: 'Sekarang negosiasi kamu sedang diperoses, yakin batalkan negosiasi?',
        action: () {
          Navigator.pop(context);
          changeStatus('empty');
        }
      )
    );
  }

  _startBergain () {
    return new Container(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
      color: Colors.white,
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _onHistoryTitle(),
          new SizedBox(height: 0),
          new Container(
            child: new Text(
              'Berikan harga terbaik kamu untuk memulai negosiasi.',
              style: TextStyle(
                color: OnColors.grey,
                fontSize: 14,
                fontWeight: FontWeight.normal
              ),
            )
          ),
          new SizedBox(height: 15),
          FieldPlusMinus(),
          new SizedBox(height: 15),
          new Container(
            child: ButtonText(
              action: () {
                _sendBergain();
              },
              title: 'Kirim Negosiasi',
              type: 'main',
            ),
          ),
        ],
      )
    );
  }

  _waitingBergain () {
    return new Container(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
      color: Colors.white,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          _onHistoryTitle(),
          new SizedBox(height: 10),
          _onHistory('Rp 15.000', 'Menunggu konfirmasi'),
          new SizedBox(height: 15),
          new Container(
            child: ButtonText(
              action: () {
                _cancelBergain();
              },
              title: 'Batalkan Negosiasi',
              type: 'grey',
            ),
          ),
        ],
      )
    );
  }

  _resultBergain (var status) {
    Widget _footer;
    if (status == 'done') {
      _footer = new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          _onHistoryTitle(),
          new SizedBox(height: 10),
          _onHistory('Rp 15.000', 'Disetujui'),
          new SizedBox(height: 15),
          new Text('Negosiasi terakhir kamu disetujui, beli sayuran ini sekarang?', style: TextStyle(
            color: OnColors.black,
            fontSize: 16,
            fontWeight: FontWeight.normal
          )),
          new SizedBox(height: 5),
          new Container(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Expanded(
                  child: ButtonText(
                    title: 'Diskusi Dulu',
                    action: () {
                      Navigator.of(context).pushNamed('chats-history');
                    },
                    type: 'grey',
                  ),
                ),
                new SizedBox(width: 10),
                new Expanded(
                  child: ButtonText(
                    title: 'Beli Sekarang',
                    action: () {
                      Navigator.of(context).pushNamed('make-order');
                    },
                    type: 'grey',
                  ),
                )
              ],
            ),
          ),
        ],
      );
    } else {
      _footer = new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          _onHistoryTitle(),
          new SizedBox(height: 10),
          _onHistory('Rp 15.000', 'Ditolak'),
          new SizedBox(height: 15),
          new Text('Negosiasi terakhir kamu ditolak, kirim negosiasi lagi?', style: TextStyle(
            color: OnColors.black,
            fontSize: 16,
            fontWeight: FontWeight.normal
          )),
          new SizedBox(height: 5),
          new Container(
            child: ButtonText(
              title: 'Kirim Negosiasi',
              action: () {
                changeStatus('empty');
              },
              type: 'grey',
            ),
          ),
        ],
      );
    }
    return new Container(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
      color: Colors.white,
      child: _footer
    );
  }

  _onHistoryTitle () {
    return new Container(
      child: new Text(
        'Negosiasi baru',
        style: TextStyle(
          color: Colors.grey.shade800,
          fontSize: 16,
          fontWeight: FontWeight.bold
        ),
      )
    );
  }

  _onHistory (var price, var status) {
    return new Card(
      elevation: (status == 'Ditolak') ? 0 : 3,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10)
      ),
      color: (status == 'Ditolak') ? OnColors.whiteGrey : Colors.white,
      margin: EdgeInsets.all(0),
      child: new Container(
        padding: EdgeInsets.all(10),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _onInfo('Negosiasi', price, OnColors.tertiary),
            new SizedBox(height: 2.5),
            _onInfo('Status', status, OnColors.black),
          ],
        ),
      ),
    );
  }

  _onInfo (var title, var value, var valColor) {
    return Container(
      margin: EdgeInsets.only(bottom: 2.5, top: 2.5),
      child: Row(
        children: <Widget>[

          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontWeight: FontWeight.normal,
                    fontSize: 16
                  ),
                )
              ],
            ),
          ),

          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  value,
                  style: TextStyle(
                    color: valColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16
                  ),
                )
              ],
            ),
          ),

        ],
      ),
    );
  }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Pengaturan',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              action: () {
                Navigator.pop(context);
                changeStatus('decline');
              },
              icon: LineIcons.minus_circle,
              type: 'white',
              title: 'Tolak negosiasi'
            ),
            new SizedBox(height: 15),
            ButtonTextIcon(
              action: () {
                Navigator.pop(context);
                changeStatus('done');
              },
              icon: LineIcons.check_circle,
              type: 'white',
              title: 'Setujui negosiasi'
            ),
            new SizedBox(height: 15),
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.trash,
              type: 'white',
              title: 'Hapus negosiasi'
            ),
            new SizedBox(height: 15),
            OnLine(),
            new SizedBox(height: 15),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

}