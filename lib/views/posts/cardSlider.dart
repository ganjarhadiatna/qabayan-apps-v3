import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class CardSlider extends StatelessWidget {
  final String image;
  final String title;
  final String price;
  final String status;

  CardSlider({
    Key key,
    this.image,
    this.title,
    this.price,
    this.status
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(right: 15),
      width: 183,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          
          new InkWell(
            onTap: () {
              Navigator.of(context).pushNamed('detail-post');
            },
            child: new Container(
              height: 183,
              margin: EdgeInsets.only(bottom: 5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: OnColors.whiteGrey,
              ),
              child: ClipRRect(
                child: Image.asset(
                  this.image,
                  fit: BoxFit.cover,
                ),
                borderRadius: new BorderRadius.circular(10),
              ),
            ),
          ),

          new InkWell(
            onTap: () {
              Navigator.of(context).pushNamed('detail-post');
            },
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new SizedBox(height: 5,),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Expanded(
                      child: new Text(
                        this.title,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: OnColors.black,
                        ),
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      height: 20,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: OnColors.secondary
                      ),
                      child: new Center(
                        child: new Text(
                          this.status,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12
                          )
                        ),
                      )
                    )
                  ],
                ),
                new SizedBox(height: 2.5,),
                new Text(
                  this.price,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: OnColors.tertiary
                  ),
                ),
              ],
            ),
          ),

        ]
      ),
    );
  }
}