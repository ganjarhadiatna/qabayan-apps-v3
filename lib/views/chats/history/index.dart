// import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
import 'package:qabayan_ui/views/auth/login.dart';
import 'package:qabayan_ui/views/chats/history/card.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChatsHistoryPage extends StatefulWidget {
  @override
  _ChatsHistoryPageState createState() => _ChatsHistoryPageState();
}

class _ChatsHistoryPageState extends State<ChatsHistoryPage> {
  String _id;

  Future getUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString('id');
    setState(() {
      this._id = id;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getUserID();
  }
  
  @override
  Widget build(BuildContext context) {
    return (this._id == null) ? LoginPage() : _onLayout();
  }

  _onLayout() {
    return new Scaffold(
      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          new Container(
                            margin: EdgeInsets.only(right: 10),
                            width: 45,
                            height: 45,
                            color: OnColors.whiteGrey,
                            child: ClipRRect(
                              borderRadius: new BorderRadius.circular(10),
                              // child: Image.asset('assets/post/2.jpg', fit: BoxFit.cover),
                            ),
                          ),

                          new Expanded(
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(
                                  'Green Tea',
                                  style: TextStyle(
                                    color: OnColors.black, 
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold
                                  ),
                                ),

                                new SizedBox(height: 2.5),

                                new Text(
                                  'Aktif 5 menit yang lalu',
                                  style: TextStyle(
                                    color: OnColors.lightGrey, 
                                    fontSize: 14),
                                ),
                              ],
                            ),
                          )
                        ]
                      )
                    ),
                    
                    new Container(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: Icons.more_vert,
                            action: () {
                              _onMore(context);
                            },
                          ),

                        ],
                      ),
                    )

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: DefaultTabController(
        length: 1,
        initialIndex: 0,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: new PreferredSize(
            preferredSize: Size(double.infinity, 138),
            child: _onCardItem(context)
          ),

          body: new Container(
            color: Colors.grey.shade100,
            child: CardChatHistoryWidget(val: 5),
          ),

          bottomNavigationBar: BottomAppBar(
            color: Colors.white,
            elevation: 0.0,
            clipBehavior: Clip.none,
            child: new Container(
              width: double.infinity,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: _onSlider(5),
                  ),
                  new Container(
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          child: ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: LineIcons.plus_circle,
                            action: () {},
                          ),
                        ),
                        new Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.multiline,
                            autofocus: false,
                            decoration: InputDecoration(
                              hintText: 'Apa yang ingin kamu tanyakan?',
                              contentPadding: EdgeInsets.fromLTRB(7.5, 15, 7.5, 15),
                              border:  OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: BorderSide.none
                              ),
                            ),
                          ),
                        ),
                        new Container(
                          child: ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: LineIcons.paper_plane,
                            action: () {},
                          ),
                        ),
                        // new Container(
                        //   width: 70,
                        //   child: ButtonText(
                        //     title: 'Kirim',
                        //     type: 'grey',
                        //     action: () {},
                        //   ),
                        // )
                      ],
                    ),
                  )
                ],
              )
            )
          )

        )
      ),

    );
  }
  
  _onCardItem (BuildContext context) {
    return new Container(
      // margin: EdgeInsets.only(left: 20, right: 20, top: 15),
      child: new Column(
        children: <Widget>[
          
          OnLine(),

          new InkWell(
            onTap: () {
              Navigator.of(context).pushNamed('detail-post');
            },
            child: new Container(
              padding: EdgeInsets.fromLTRB(20, 15, 20, 10),
              color: Colors.white,
              child: new Row(
                children: <Widget>[

                  new Container(
                    margin: EdgeInsets.only(right: 10),
                    width: 40,
                    height: 40,
                    color: OnColors.whiteGrey,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(5),
                      child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                    ),
                  ),

                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          'Cabai rawit merah',
                          style: TextStyle(
                            color: Colors.grey.shade800,
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        new SizedBox(height: 2.5),
                        new Text(
                          'IDR 45 K',
                          style: TextStyle(
                            color: OnColors.tertiary,
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),

                  new Container(
                    // width: 40,
                    child: ButtonCircle(
                      action: () {},
                      icon: LineIcons.close,
                      size: 26.0,
                      type: 'white',
                    ),
                  )

                ],
              ),
            ),
          ),

          new Container(
            padding: EdgeInsets.only(left: 20, bottom: 15, right: 20),
            color: Colors.white,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Expanded(
                  child: ButtonText(
                    title: 'Negosiasi Harga',
                    action: () {
                      Navigator.of(context).pushNamed('bergains-order');
                    },
                    type: 'grey',
                  ),
                ),
                new SizedBox(width: 10),
                new Expanded(
                  child: ButtonText(
                    title: 'Beli Sayuran',
                    action: () {
                      Navigator.of(context).pushNamed('make-order');
                    },
                    type: 'grey',
                  ),
                )
              ],
            ),
          ),

        ],
      ),
    );
  }

  _onSlider (val) {
    return new Container(
      height: 36,
      child: new ListView(
        scrollDirection: Axis.horizontal,
        children: List.generate(val, (index) {
          return new Container(
            margin: EdgeInsets.only(left: 5, right: 5),
            padding: EdgeInsets.only(left: 15, top: 5, right: 15, bottom: 5),
            // width: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(22),
              color: OnColors.primary,
            ),
            child: new Center(
              child: new Text(
                'Dunungan, barangnya masih ada?',
                style: TextStyle(
                  color: OnColors.black,
                  fontSize: 14
                )
              ),
            )
          );
        })
      ),
    );
  }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Obrolan',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.trash,
              type: 'white',
              title: 'Hapus obrolan'
            ),
            new SizedBox(height: 15),
            OnLine(),
            new SizedBox(height: 15),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

  // _onSales (BuildContext context) {
  //   return showDialog(
  //     context: context,
  //     builder: (BuildContext context) => PopupQuestion(
  //       title: 'Konfirmasi',
  //       subtitle: 'Jika data sudah benar, jual sayuran sekarang?',
  //       action: () {
  //         Navigator.pop(context);
  //         showDialog(
  //           context: context,
  //           builder: (BuildContext context) => PopupAlert(
  //             title: 'Pemberitahuan',
  //             subtitle: 'Sayuran kamu berhasil dipublikasi.',
  //             action: () {
  //               Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => false);
  //             },
  //           )
  //         );
  //       }
  //     )
  //   );
  // }

}