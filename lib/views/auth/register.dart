import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/loading.dart';

// // services
import 'package:qabayan_ui/modules/Session.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  // var
  static var _messageErorr = 'Message error';
  
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  // bool _isLoading = false;
  bool _isError = false;
  bool _emailFocus = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Daftar'),

                  ],
                )
              )
            )

          ],
        ),
      ),
      body: new ListView(
        children: <Widget>[

          new SizedBox(height: 30),

          new Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: new Text(
              'Daftar',
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.grey.shade800,
                fontWeight: FontWeight.bold,
                fontSize: 56,
              ),
            ),
          ),

          new Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: new Text(
              'Kamu bisa daftar dengan memasukan data yang diperlukan, atau dengan akun facebook dan goole.',
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.grey.shade600,
                fontWeight: FontWeight.normal,
                fontSize: 16,
              ),
            ),
          ),
          
          new SizedBox(height: 50),

          // error
          new Center(
            child: _isError ? new Container(
              margin: EdgeInsets.only(bottom: 10),
              child: new Text(
                _messageErorr,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: OnColors.red,
                  fontSize: 16,
                  fontWeight: FontWeight.bold
                ),
              ),
            ) : new Container()
          ),

          new Container(
            margin: EdgeInsets.only(bottom: 5, left: 20, right: 20),
            child: _onTitleField('Nama lengkap'),
          ),
          new Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: new TextFormField(
              keyboardType: TextInputType.text,
              autofocus: false,
              controller: _nameController,
              decoration: new InputDecoration(
                hintText: 'Indry Wahyuni',
                contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
                border:  OutlineInputBorder(
                  borderRadius: BorderRadius.circular(7.5),
                ),
              )
            ),
          ),

          new SizedBox(height: 15),

          new Container(
            margin: EdgeInsets.only(bottom: 5, left: 20, right: 20),
            child: _onTitleField('Alamat e-mail'),
          ),
          new Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: new TextFormField(
              keyboardType: TextInputType.emailAddress,
              autofocus: false,
              controller: _emailController,
              decoration: new InputDecoration(
                hintText: 'alamat-email@qabayan.com',
                contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
                border:  OutlineInputBorder(
                  borderRadius: BorderRadius.circular(7.5),
                ),
              )
            ),
          ),

          new SizedBox(height: 15),

          new Container(
            margin: EdgeInsets.only(bottom: 5, left: 20, right: 20),
            child: _onTitleField('Buat password'),
          ),
          new Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: new TextFormField(
              keyboardType: TextInputType.text,
              obscureText: true,
              autofocus: false,
              controller: _passwordController,
              decoration: new InputDecoration(
                hintText: 'Password',
                contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
                border:  OutlineInputBorder(
                  borderRadius: BorderRadius.circular(7.5),
                ),
              )
            ),
          ),

          new SizedBox(height: 30),

          // button
          new Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            width: double.infinity,
            child: ButtonText(
              action: () {
                _onRegister();
              },
              type: 'main',
              title: 'Buat Akun',
            ),
          ),

          new SizedBox(height: 15),

          new Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: new Column(
              children: <Widget>[
                new Text(
                  'Atau daftar dengan',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: Colors.grey.shade600,
                    fontWeight: FontWeight.normal,
                    fontSize: 16,
                  ),
                ),
                new SizedBox(height: 15),
                new Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Expanded(
                      child: ButtonText(
                        action: () {},
                        type: 'grey',
                        title: 'Facebook',
                      ),
                    ),
                    new SizedBox(width: 20),
                    new Expanded(
                      child: ButtonText(
                        action: () {},
                        type: 'grey',
                        title: 'Google',
                      ),
                    )
                  ],
                )
              ],
            )
          ),

          new SizedBox(height: 30),

          new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(
                  'Udah pernah buat akun? ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.grey.shade600,
                    fontSize: 16,
                  ),
                ),
                new InkWell(
                  onTap: () {
                    Navigator.of(context).popAndPushNamed('login');
                  },
                  child: new Text(
                    'Tinggal login.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: OnColors.main,
                      fontSize: 16,
                      fontWeight: FontWeight.bold
                    ),
                  )
                ),
              ],
            ),
          )

        ],
      ),
    );
  }

  _onTitleField (title) {
    return Container(
      child: new Text(
        title, 
        style: TextStyle(
          fontWeight: FontWeight.normal,
          fontSize: 14.0,
          color: Colors.grey.shade800
        ),
        softWrap: true
      ),
    );
  }

  _onLoading () {
    return showDialog(
      context: context,
      barrierDismissible: false,
      child: Loading()
    );
  }

  _onRegister () async {
    var name = _nameController.text;
    var email = _emailController.text;
    var password = _passwordController.text;

    setState(() {
      _emailFocus = false;
      _isError = false;
    });

    if (name == '' || email == '' || password == '') {
      
      setState(() {
        _emailFocus = true;
        _isError = true;
        _messageErorr = 'Nama, email & password harus diisi';
      });

    } else {
      _onLoading();
      Session.setUserToken('token');
      Session.setUserID('1');
      Session.setUserUsername(email);

      Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => false);
    }
  }
}