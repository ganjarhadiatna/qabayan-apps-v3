// import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onColors.dart';
// import 'package:qabayan_ui/assets/popupAlert.dart';
// import 'package:qabayan_ui/assets/popupQuestion.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
import 'package:qabayan_ui/views/bergains/sales/card.dart';

class BergainSalePage extends StatefulWidget {
  @override
  _BergainSalePageState createState() => _BergainSalePageState();
}

class _BergainSalePageState extends State<BergainSalePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Negosiasi Penjualan'),
                    
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Container(
                            // width: 40,
                            padding: EdgeInsets.only(left: 7.5, right: 7.5),
                            height: 25,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: OnColors.secondary
                            ),
                            child: new Center(
                              child: new Text(
                                'Masih Dijual',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 13,
                                )
                              )
                            ),
                          ),
                          new SizedBox(width: 5),
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: Icons.more_vert,
                            action: () {
                              _onMore(context);
                            },
                          ),

                        ],
                      ),
                    )

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: DefaultTabController(
        length: 1,
        initialIndex: 0,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: new PreferredSize(
            preferredSize: Size(double.infinity, 70),
            child: _onCardItem(context)
          ),

          body: new Container(
            color: Colors.grey.shade100,
            child: CardBergainSalesWidget(val: 5),
          ),

        )
      ),

    );
  }
  
  _onCardItem (BuildContext context) {
    return new Container(
      // margin: EdgeInsets.only(left: 20, right: 20, top: 15),
      child: new Column(
        children: <Widget>[

          OnLine(),

          new InkWell(
            onTap: () {
              Navigator.of(context).pushNamed('detail-post');
            },
            child: new Container(
              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
              color: Colors.white,
              child: new Row(
                children: <Widget>[

                  new Container(
                    margin: EdgeInsets.only(right: 10),
                    width: 40,
                    height: 40,
                    color: OnColors.whiteGrey,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(5),
                      child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                    ),
                  ),

                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          'Cabai rawit merah',
                          style: TextStyle(
                            color: Colors.grey.shade800,
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        new SizedBox(height: 2.5),
                        new Text(
                          'Rp. 15,000',
                          style: TextStyle(
                            color: OnColors.tertiary,
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),

                  new Container(
                    // width: 40,
                    child: ButtonCircle(
                      action: () {
                        Navigator.of(context).pushNamed('detail-post');
                      },
                      icon: Icons.chevron_right,
                      size: 32.0,
                      type: 'white',
                    ),
                  )

                ],
              ),
            ),
          ),

          // new Container(
          //   padding: EdgeInsets.only(left: 20, bottom: 15, right: 20),
          //   color: Colors.white,
          //   child: new Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: <Widget>[
          //       new Expanded(
          //         child: ButtonText(
          //           title: 'Nego Barang',
          //           action: () {},
          //           type: 'grey',
          //         ),
          //       ),
          //       new SizedBox(width: 10),
          //       new Expanded(
          //         child: ButtonText(
          //           title: 'Beli Barang',
          //           action: () {
          //             Navigator.of(context).pushNamed('make-order');
          //           },
          //           type: 'main',
          //         ),
          //       )
          //     ],
          //   ),
          // ),

        ],
      ),
    );
  }

  // _onSlider (val) {
  //   return new Container(
  //     height: 36,
  //     child: new ListView(
  //       scrollDirection: Axis.horizontal,
  //       children: List.generate(val, (index) {
  //         return new Container(
  //           margin: EdgeInsets.only(left: 5, right: 5),
  //           padding: EdgeInsets.only(left: 15, top: 5, right: 15, bottom: 5),
  //           // width: 200,
  //           decoration: BoxDecoration(
  //             borderRadius: BorderRadius.circular(22),
  //             color: OnColors.primary,
  //           ),
  //           child: new Center(
  //             child: new Text(
  //               'Dunungan, barangnya masih ada?',
  //               style: TextStyle(
  //                 color: OnColors.black,
  //                 fontSize: 14
  //               )
  //             ),
  //           )
  //         );
  //       })
  //     ),
  //   );
  // }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Nego',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.trash,
              type: 'white',
              title: 'Hapus nego'
            ),
            new SizedBox(height: 15),
            OnLine(),
            new SizedBox(height: 15),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

  // _onSales (BuildContext context) {
  //   return showDialog(
  //     context: context,
  //     builder: (BuildContext context) => PopupQuestion(
  //       title: 'Konfirmasi',
  //       subtitle: 'Jika data sudah benar, jual sayuran sekarang?',
  //       action: () {
  //         Navigator.pop(context);
  //         showDialog(
  //           context: context,
  //           builder: (BuildContext context) => PopupAlert(
  //             title: 'Pemberitahuan',
  //             subtitle: 'Sayuran kamu berhasil dipublikasi.',
  //             action: () {
  //               Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => false);
  //             },
  //           )
  //         );
  //       }
  //     )
  //   );
  // }

}