import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
// import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/views/orders/cardMethod.dart';

class EstimateMethodPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,

      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    TitleBar(title: 'Metode Pembayaran'),
                    
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          ButtonCircle(
                            size: 28.0,
                            type: 'white',
                            icon: Icons.more_vert,
                            action: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) => PopupBottom(
                                  title: 'Beli Barang',
                                  content: new Column(
                                    children: <Widget>[
                                      ButtonText(
                                        action: () {},
                                        type: 'primary',
                                        title: 'Simpan Perubahan'
                                      ),
                                      new SizedBox(height: 15,),
                                      ButtonText(
                                        action: () {
                                          Navigator.pop(context);
                                        },
                                        type: 'grey',
                                        title: 'Tutup'
                                      ),
                                    ]
                                  ),
                                ),
                              );
                            },
                          ),

                        ],
                      ),
                    )

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new Container(
        color: Colors.grey.shade100,
        child: new ListView(
          padding: EdgeInsets.only(top: 15),
          children: <Widget>[
            CardMethod(
              title: '1 Hari', 
              subtitle: 'Estimasi waktu',
              icon: Icons.check),
            CardMethod(
              title: '2 Hari', 
              subtitle: 'Estimasi waktu',
              icon: Icons.check),
            CardMethod(
              title: '3 Hari', 
              subtitle: 'Estimasi waktu',
              icon: Icons.check),
            CardMethod(
              title: '4 Hari', 
              subtitle: 'Estimasi waktu',
              icon: Icons.check),
          ],
        ),
      ),

      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        elevation: 0.0,
        clipBehavior: Clip.none,
        child: new Container(
          width: double.infinity,
          padding: EdgeInsets.all(15),
          child: ButtonText(
            title: 'Simpan Perubahan',
            type: 'main',
            action: () {
              Navigator.pop(context);
            },
          ),
        )
      )

    );
  }

}