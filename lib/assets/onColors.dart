import 'package:flutter/material.dart';

class OnColors {
  // others
  static const teal = Colors.teal;
  static const red = Color.fromRGBO(213, 63, 46, 1);
  static const magenta = Color.fromRGBO(232, 11, 63, 1);
  static const blue = Color.fromRGBO(46, 143, 213, 1);

  // first
  static const main = Color.fromRGBO(54, 181, 176, 1);
  static const primary = Color.fromRGBO(252, 245, 176, 1);
  static const secondary = Color.fromRGBO(157, 216, 200, 1);
  static const tertiary = Color.fromRGBO(240, 149, 149, 1);

  // second
  static const black = Color.fromRGBO(4, 4, 4, 0.8);
  static const grey = Color.fromRGBO(85, 85, 85, 0.7);
  static const lightGrey = Color.fromRGBO(153, 153, 153, 1);
  static const whiteGrey = Color.fromRGBO(245, 245, 245, 1);
}