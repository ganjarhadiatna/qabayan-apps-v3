import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class FieldPlusMinus extends StatefulWidget {
  // TextEditingController _numberController = TextEditingController();

  @override
  _FieldPlusMinusState createState() => _FieldPlusMinusState();
}

class _FieldPlusMinusState extends State<FieldPlusMinus> {
  var value;

  final _valueController = TextEditingController();

  void initState() {
    _valueController.text = "0";
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: _onPlusMinus(),
    );
  }

  _onPlusMinus () {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        // _cardNotes('Jumlah barang yang ingin dipesan.'),
        new Container(
          // margin: EdgeInsets.only(top: 10, bottom: 10),
          width: double.maxFinite,
          height: 45,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(
              color: Colors.grey.shade500
            ),
          ),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              _onSmallButton(LineIcons.minus, () {
                int newVal = 0;
                if (_valueController.text != "0") {
                  newVal = int.parse(_valueController.text);
                  newVal--;
                  _valueController.text = (newVal).toString();
                } else {
                  _valueController.text = "0";
                }
              }),

              // new SizedBox(width: 10),

              new Expanded(
                child: TextFormField(
                  keyboardType: TextInputType.numberWithOptions(
                    decimal: false,
                    signed: true
                  ),
                  autofocus: false,
                  textAlign: TextAlign.center,
                  controller: _valueController,
                  decoration: InputDecoration(
                    fillColor: OnColors.grey,
                    hintText: '0',
                    // border: InputBorder.none,
                    contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    border:  OutlineInputBorder(
                      borderRadius: BorderRadius.circular(0),
                      borderSide: BorderSide.none,
                    ),
                  ),
                ),
              ),

              // new SizedBox(width: 10),

              _onSmallButton(LineIcons.plus, () {
                int newVal = 0;
                if (_valueController.text != "") {
                  newVal = int.parse(_valueController.text);
                  newVal++;
                  _valueController.text = (newVal).toString();
                } else {
                  _valueController.text = "0";
                }
              }),

            ],
          ),
        ),
      ],

    );
  }

  _onSmallButton (var icon, var action) {
    return new Container(
      // borderRadius: BorderRadius.all(Radius.circular(0)),
      width: 45,
      child: new MaterialButton(
        // minWidth: 35,
        height: 30,
        padding: EdgeInsets.only(left: 0, right: 0),
        color: Colors.transparent,
        elevation: 0,
        onPressed: action,
        child: new Center(
          child: Icon(
            icon,
            size: 22,
            color: OnColors.grey,
          ),
        ),
      )
    );
  }
}
