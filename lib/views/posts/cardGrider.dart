import 'package:flutter/material.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class CardGriderWidget extends StatelessWidget {
  final String image;
  final String title;
  final String price;
  final String status;

  CardGriderWidget({
    Key key,
    this.image,
    this.title,
    this.price,
    this.status
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Card(
      elevation: 0,
      color: Colors.white,
      margin: EdgeInsets.all(5),
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10)
      ),
      child: new InkWell(
        onTap: () {
          Navigator.of(context).pushNamed('detail-post');
        },
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[

            new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  width: double.infinity,
                  height: 170,
                  margin: EdgeInsets.all(5),
                  child: ClipRRect(
                    child: Image.asset(
                      this.image,
                      fit: BoxFit.cover,
                    ),
                    borderRadius: new BorderRadius.circular(10),
                  ),
                ),

                new Container(
                  margin: EdgeInsets.all(5),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Expanded(
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text(
                              this.title,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.normal,
                                color: OnColors.black,
                              ),
                            ),
                            new Text(
                              this.price,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: OnColors.tertiary
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only(left: 10),
                        width: 50,
                        height: 22,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(22),
                          color: OnColors.secondary
                        ),
                        child: new Center(
                          child: new Text(
                            this.status,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12
                            )
                          ),
                        )
                      )
                    ]
                  )
                )
              ],
            ),

            new Container(
              margin: EdgeInsets.all(5),
              child: ButtonText(
                action: () {
                  Navigator.of(context).pushNamed('make-order');
                },
                title: 'Beli Sayuran',
                type: 'main',
              )
            )

          ],
        ),
      ),
    );
    // return new Container(
    //   width: double.infinity,
    //   child: new Column(
    //     crossAxisAlignment: CrossAxisAlignment.start,
    //     children: <Widget>[
          
    //       new InkWell(
    //         onTap: () {
    //           Navigator.of(context).pushNamed('detail-post');
    //         },
    //         child: new Container(
    //           height: 183,
    //           margin: EdgeInsets.only(bottom: 5),
    //           decoration: BoxDecoration(
    //             borderRadius: BorderRadius.circular(10),
    //             color: OnColors.whiteGrey,
    //           ),
    //           child: ClipRRect(
    //             child: Image.asset(
    //               this.image,
    //               fit: BoxFit.cover,
    //             ),
    //             borderRadius: new BorderRadius.circular(10),
    //           ),
    //         ),
    //       ),

    //       new InkWell(
    //         onTap: () {
    //           Navigator.of(context).pushNamed('detail-post');
    //         },
    //         child: new Column(
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           children: <Widget>[
    //             new SizedBox(height: 5,),
    //             new Row(
    //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //               crossAxisAlignment: CrossAxisAlignment.center,
    //               children: <Widget>[
    //                 new Expanded(
    //                   child: new Text(
    //                     this.title,
    //                     style: TextStyle(
    //                       fontSize: 18,
    //                       fontWeight: FontWeight.normal,
    //                       color: OnColors.black,
    //                     ),
    //                   ),
    //                 ),
    //                 new Container(
    //                   margin: EdgeInsets.only(left: 10),
    //                   width: 50,
    //                   height: 22,
    //                   decoration: BoxDecoration(
    //                     borderRadius: BorderRadius.circular(22),
    //                     color: OnColors.secondary
    //                   ),
    //                   child: new Center(
    //                     child: new Text(
    //                       this.status,
    //                       style: TextStyle(
    //                         color: Colors.white,
    //                         fontSize: 12
    //                       )
    //                     ),
    //                   )
    //                 )
    //               ],
    //             ),
    //             new SizedBox(height: 2.5,),
    //             new Text(
    //               this.price,
    //               style: TextStyle(
    //                 fontSize: 16,
    //                 fontWeight: FontWeight.bold,
    //                 color: OnColors.tertiary
    //               ),
    //             ),
    //           ],
    //         ),
    //       ),

    //     ]
    //   ),
    // );
  }
}