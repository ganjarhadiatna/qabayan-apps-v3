import 'package:flutter/material.dart';

class PopupLoading extends StatelessWidget {
  final title;
  final action;

  PopupLoading({
    Key key,
    @required this.title,
    this.action
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
      alignment: Alignment(0,0),
      child: new Container(
        width: 300,
        padding: EdgeInsets.all(20),
        margin: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15)
        ),
        child: new Material(
          color: Colors.transparent,
          child: new InkWell(
            onTap: this.action,
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[

                new CircularProgressIndicator(strokeWidth: 2),
                new SizedBox(width: 15),
                new Expanded(
                  child: new Text(
                    this.title,
                    style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 16,
                      fontWeight: FontWeight.normal
                    ),
                  ),
                )

              ],
            ),
          ),
        ),
      )
    );
  }

}
