import 'package:flutter/material.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/onColors.dart';

class CardSearch extends StatelessWidget {

  final title;
  final icon;
  final action;

  CardSearch({
    Key key,
    this.title,
    this.icon,
    this.action
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: this.action,
      child: new Container(
        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        // margin: EdgeInsets.fromLTRB(0, 5, 0, 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(0),
          color: Colors.white,
        ),
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[

            new Container(
                margin: EdgeInsets.only(right: 10),
                width: 40,
                height: 40,
                // color: Colors.grey.shade100,
                child: ClipRRect(
                  borderRadius: new BorderRadius.circular(40),
                  child: new Center(
                    child: new Icon(
                      this.icon,
                      size: 24,
                      color: OnColors.main,
                    ),
                  ),
                ),
            ),

            new Expanded(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    this.title,
                    style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 16,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ],
              ),
            ),

            // ButtonCircle(
            //   size: 22.0,
            //   type: 'white',
            //   icon: this.icon,
            //   action: () {},
            // ),

          ],
        ),
      ),
    );
  }
}