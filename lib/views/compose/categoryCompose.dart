import 'dart:async';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/titleBar.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/views/orders/cardMethod.dart';
import 'package:qabayan_ui/services/category.dart';

class CategoryComposePage extends StatefulWidget {
  @override
  _CategoryComposePageState createState() => _CategoryComposePageState();
}

class _CategoryComposePageState extends State<CategoryComposePage> {
  List dataCategory;

  Future getDataCategory() async {
    var dataCategory = await loadCategory();
    setState(() {
      this.dataCategory = dataCategory;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getDataCategory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: new PreferredSize(
        preferredSize: new Size.fromHeight(70),
        child: new AppBar(
          elevation: 0.0,
          leading: new Container(),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Expanded(
              child: new Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 12.5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    ButtonCircle(
                      size: 32.0,
                      type: 'white',
                      icon: Icons.chevron_left,
                      action: () {
                        Navigator.pop(context);
                      }
                    ),

                    new Expanded(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TitleBar(title: 'Kategori Barang'),
                          new Text(
                            'Pilih satu kategori',
                            style: new TextStyle(
                              color: OnColors.grey,
                              fontSize: 14
                            ),
                          )
                        ],
                      )
                    )

                  ],
                )
              )
            )

          ],
        ),
      ),

      body: new Container(
        color: Colors.grey.shade100,
        child: new ListView.builder(
          padding: EdgeInsets.only(top: 15),
          itemCount: this.dataCategory.length,
          itemBuilder: (context, index) {
            return new CardMethod(
              title: this.dataCategory[index].title,
              subtitle: this.dataCategory[index].totalItem + ' barang',
              icon: LineIcons.check,
            );
          }
        ),
      ),

      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        elevation: 0.0,
        clipBehavior: Clip.none,
        child: new Container(
          width: double.infinity,
          padding: EdgeInsets.all(15),
          child: ButtonText(
            title: 'Simpan Perubahan',
            type: 'main',
            action: () {
              Navigator.pop(context);
            },
          ),
        )
      )
    );
  }

  // _onTitle (var title) {
  //   return Container (
  //     margin: EdgeInsets.fromLTRB(20, 0, 20, 15),
  //     child: Text(
  //       title,
  //       style: TextStyle(
  //         color: OnColors.black,
  //         fontSize: 16,
  //         fontWeight: FontWeight.normal,
  //       )
  //     ),
  //   );
  // }

}