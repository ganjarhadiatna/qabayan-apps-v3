import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
// import 'package:line_icons/line_icons.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/onColors.dart';
import 'package:qabayan_ui/assets/onLine.dart';
// import 'package:qabayan_ui/views/posts/index.dart';
// import 'package:qabayan_ui/views/users/items/solds.dart';

class CardSalesWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new ListView.builder(
        padding: EdgeInsets.only(top: 10, bottom: 10),
        itemCount: 1,
        itemBuilder: (context, position) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _onCard(context, 'Menunggu persetujuan', 'Setujui Pesanan'),
              _onCard(context, 'Menunggu pesanan', 'Antarkan Pesanan'),
              _onCard(context, 'Pesanan diterima', 'Transaksi Selesai'),
              _onCard(context, 'Pesanan berhasil', ''),
              _onCard(context, 'Pesanan berhasil', ''),
              // new SizedBox(height: 60)
            ],
          );
        },
      ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // floatingActionButton: new Container(
      //     width: 170,
      //     child: new RaisedButton(
      //       onPressed: () {
      //         Navigator.of(context).push(
      //           MaterialPageRoute(
      //             builder: (context) => SoldsPage()
      //           )
      //         );
      //       },
      //       shape: RoundedRectangleBorder(
      //         borderRadius: BorderRadius.circular(35)
      //       ),
      //       elevation: 3,
      //       // disabledElevation: 1,
      //       highlightElevation: 0,
      //       child: new Row(
      //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //         children: <Widget>[
      //           new Text(
      //             'Riwayat Penjualan',
      //             style: TextStyle(
      //               color: Colors.grey.shade800
      //             )
      //           ),
      //           new Icon(
      //             Icons.chevron_right,
      //             color: Colors.grey.shade800
      //           )
      //         ],
      //       ),
      //       color: Colors.white,
      //     ),
      // )
    );
  }

  _onCard (BuildContext context, var status, var btnText) {

    Widget _orderButton;

    if (btnText != '') {

      _orderButton = new Container(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: 15),
        child: new ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: double.infinity
          ),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              new Expanded(
                child: ButtonText(
                  action: () {
                    Navigator.of(context).pushNamed('detail-sales');
                  },
                  type: 'grey',
                  title: 'Detail Pesanan'
                ),
              ),

              new SizedBox(width: 10),

              new Expanded(
                child: ButtonText(
                  action: () {},
                  type: 'main',
                  title: btnText
                ),
              ),

            ],
          ),
        ),
      );

    } else {

      _orderButton = new Container(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: 15),
        child: new ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: double.infinity
          ),
          child: ButtonText(
            action: () {
              Navigator.of(context).pushNamed('detail-sales');
            },
            type: 'grey',
            title: 'Detail Pesanan'
          ),
        ),
      );

    }
    
    return new Card(
            
      margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
      semanticContainer: true,
      elevation: 0.0,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0)
      ),

      child: new Container(
        // padding: EdgeInsets.fromLTRB(20, 15, 20, 15),

        child: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,

          children: <Widget>[

            new Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed('user-profile');
                    },
                    child: new Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 35,
                      height: 35,
                      color: OnColors.whiteGrey,
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(5),
                        // child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  new Container(
                    child: new Text(
                      'Green Tea',
                      style: TextStyle(
                        color: OnColors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.normal
                      )
                    ),
                  ),
                  new Expanded(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        ButtonCircle(
                          action: () {
                            Navigator.of(context).pushNamed('chats-history');
                          },
                          icon: LineIcons.comments,
                          size: 22.0,
                          type: "white",
                        ),
                        ButtonCircle(
                          action: () {
                            Navigator.of(context).pushNamed('user-profile');
                          },
                          icon: Icons.chevron_right,
                          size: 28.0,
                          type: 'white',
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),

            OnLine(),

            new Container(
              padding: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[

                  new Container(
                    margin: EdgeInsets.only(right: 10),
                    width: 80,
                    height: 80,
                    color: OnColors.whiteGrey,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(5),
                      child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
                    ),
                  ),

                  new Expanded(
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Cabai rawit merah',
                            style: TextStyle(
                              color: Colors.grey.shade800,
                              fontSize: 16,
                              fontWeight: FontWeight.bold
                            ),
                          ),

                          new SizedBox(height: 2.5),
                          // _onInfo ('Pembayaran', 'Dibayar hari ini', Colors.grey.shade800),
                          // _onInfo ('Pengiriman', 'Diambil oleh pembeli', Colors.grey.shade800),
                          _onInfo ('Total biaya', 'Rp 26.000', OnColors.tertiary),
                          _onInfo ('Status pesanan', status, OnColors.black),
                          new SizedBox(height: 5),

                        ],
                      ),
                  ),

                ],
              ),
            ),
            
            _orderButton,

          ],
        ),
      ),
    );
  }

  _onInfo (var title, var value, var valColor) {
    return new Container(
      width: double.infinity,
      margin: EdgeInsets.only(top: 7.5),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[

          new Text(
            title,
            style: TextStyle(
              color: Colors.grey.shade800,
              fontWeight: FontWeight.normal,
              fontSize: 16
            ),
          ),

          new Text(
            value,
            style: TextStyle(
              color: valColor,
              fontWeight: FontWeight.bold,
              fontSize: 16
            ),
          ),

        ],
      ),
    );
  }


}