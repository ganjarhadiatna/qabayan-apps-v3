import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/buttonText.dart';
import 'package:qabayan_ui/assets/buttonTextIcon.dart';
// import 'package:qabayan_ui/posts/index.dart';
// import 'package:qabayan_ui/assets/buttonCircle.dart';
import 'package:qabayan_ui/assets/onLine.dart';
import 'package:qabayan_ui/assets/onColors.dart';
// import 'package:qabayan_ui/assets/popupAlert.dart';
import 'package:qabayan_ui/assets/popupBottom.dart';
// import 'package:qabayan_ui/assets/popupQuestion.dart';

class CardBergainOrderWidget extends StatelessWidget {

  final int val;

  CardBergainOrderWidget({Key key, @required this.val}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        new SizedBox(height: 15),
        _onCardLeft(context, 1),
        _onCardLeft(context, 2),
        _onCardLeft(context, 3),
        _onCardLeft(context, 3),
        new SizedBox(height: 65),
      ],
    );
  }


  _onCardLeft (BuildContext context, val) {

    return new InkWell(
      onLongPress: () {
        _onMore(context);
      },
      child: new Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(20, 0, 20, 15),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            new Container(
              margin: EdgeInsets.only(right: 10),
              width: 40,
              height: 40,
              color: Colors.white,
              child: ClipRRect(
                borderRadius: new BorderRadius.circular(5),
                // child: Image.asset('assets/post/1.jpg', fit: BoxFit.cover),
              ),
            ),
            
            new Expanded(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                    margin: EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      )
                    ),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text('Green Tea', style: TextStyle(
                          color: OnColors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold
                        )),
                        new SizedBox(height: 5),
                        _onInfo('Negosiasi', 'IDR 50 K', OnColors.tertiary),
                        new SizedBox(height: 2.5),
                        _onInfo('Status', val == 1 ? 'menunggu' : val == 2 ? 'Ditolak' : val == 3 ? 'Disetujui' : '', OnColors.black),

                        val == 1 ? (new Container()) : val == 2 ? (_onFailedFooter(context)) : val == 3 ? (_onDoneFooter(context)) : new Container()
                        
                      ],
                    ),
                  ),

                  new Container(
                    child: new Text(
                      '5 menit yang lalu',
                      style: TextStyle(color: OnColors.lightGrey, fontSize: 14),
                    ),
                  )
                ],
              ),
            )

          ],
        ),
      )
    );
  }

  _onFailedFooter (BuildContext context) {
    return new Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new SizedBox(height: 15),

          new Text('Negosiasi ditolak, nego lagi?', style: TextStyle(
            color: OnColors.black,
            fontSize: 16,
            fontWeight: FontWeight.normal
          )),
          new SizedBox(height: 5),
          new Container(
            child: ButtonText(
              title: 'Nego Harga',
              action: () {},
              type: 'grey',
            ),
          )
        ],
      ),
    );
  }

  _onDoneFooter (BuildContext context) {
    return new Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new SizedBox(height: 15),

          new Text('Negosiasi disetujui, beli sekarang?', style: TextStyle(
            color: OnColors.black,
            fontSize: 16,
            fontWeight: FontWeight.normal
          )),
          new SizedBox(height: 5),
          new Container(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Expanded(
                  child: ButtonText(
                    title: 'Ngobrol',
                    action: () {
                      Navigator.of(context).pushNamed('chats-history');
                    },
                    type: 'grey',
                  ),
                ),
                new SizedBox(width: 10),
                new Expanded(
                  child: ButtonText(
                    title: 'Beli Sayuran',
                    action: () {
                      Navigator.of(context).pushNamed('make-order');
                    },
                    type: 'grey',
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  _onInfo (var title, var value, var valColor) {
    return new Container(
      width: double.infinity,
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[

          new Text(
            title,
            style: TextStyle(
              color: Colors.grey.shade800,
              fontWeight: FontWeight.normal,
              fontSize: 16
            ),
          ),

          new Text(
            value,
            style: TextStyle(
              color: valColor,
              fontWeight: FontWeight.normal,
              fontSize: 16
            ),
          ),

        ],
      ),
    );
  }

  _onMore (BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => PopupBottom(
        title: 'Pengaturan',
        content: new Column(
          children: <Widget>[
            ButtonTextIcon(
              action: () {
                Navigator.of(context).pushNamed('user-profile');
              },
              icon: LineIcons.user,
              type: 'white',
              title: 'Lihat profil'
            ),
            new SizedBox(height: 15),
            ButtonTextIcon(
              action: () {},
              icon: LineIcons.trash,
              type: 'white',
              title: 'Hapus nego'
            ),
            new SizedBox(height: 15),
            OnLine(),
            new SizedBox(height: 15),
            ButtonText(
              action: () {
                Navigator.pop(context);
              },
              type: 'grey',
              title: 'Tutup'
            ),
          ]
        ),
      ),
    );
  }

}